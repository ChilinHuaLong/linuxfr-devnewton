URL:     https://linuxfr.org/news/entretien-avec-fabien-sanglard-a-propos-du-cps-1
Title:   Entretien avec Fabien Sanglard à propos du CPS-1
Authors: devnewton
         BAud, Christophe, Julien Jorge, orfenor, tisaac et Ysabeau
Date:    2022-10-09T15:04:50+02:00
License: CC By-SA
Tags:    cps, sdk, capcom et retrogaming
Score:   54


Fabien Sanglard a publié récemment [un SDK pour créer des jeux pour le système de Capcom](https://linuxfr.org/users/devnewton/liens/un-sdk-pour-creer-des-jeux-pour-cps-1), cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à ces bornes d’arcade au point de publier [un livre sur le sujet](https://fabiensanglard.net/cpsb/index.html).


C’est l’occasion de revenir dans les années 80-90 là où les bornes d’arcade proliféraient :-) (j’vous prends au baby quand vous l’voul’ :p)

NdM : la lecture de cette dépêche est compatible avec les jeans, les blousons en cuir marron et les coupes mulets.


&nbsp;|&nbsp;
-----------------|-----------
![couverture du livre](cover.webp) | ![arcade](298px-Donkey_Kong_arcade.jpg)

----

[Code du SDK pour borne d'arcade Capcom](https://github.com/fabiensanglard/ccps)
[Le bouquin The Book of CP-System](https://fabiensanglard.net/cpsb/index.html)
[En savoir plus sur le gars](https://fabiensanglard.net/about/)
[CPS et tous ses jeux chez wikipedia](https://en.wikipedia.org/wiki/CP_System)

----

### Qui êtes-vous, quel est votre parcours et pourquoi est-il aussi lié aux jeux vidéos ?


Je suis originaire de Romans-sur-Isère dans la Drome. Un joli coin avec une vue sur le Vercors magnifique. J’ai surtout passé mon temps à jouer aux jeux vidéo et au foot. Parcours banal avec collège, lycée, Bac S, Miage à Grenoble et ensuite Master.  Ensuite, je voulais voir le monde, je suis parti à Paris un an, puis au Canada en [PVT](https://fr.wikipedia.org/wiki/Programme_Vacances_Travail) pour y rester six mois. J’y suis resté dix ans. Ultérieurement, j’ai reçu une offre d’emploi de la part d’un employeur dont je rêvais depuis plusieurs années. Du coup, j’ai déménagé aux USA, ou je vis encore aujourd’hui.

La fascination pour les jeux vidéo, c’est en grande partie lié à mes années d’adolescence où c’était dur d’accéder à une expérience idéale. Mon premier ordinateur était un [ZX Spectrum](https://www.grospixels.com/site/sinclair.php), ensuite un [Amstrad CPC](https://www.grospixels.com/site/amstrad.php), et ensuite un [Atari ST](https://www.grospixels.com/site/atarist.php).
Toutes ces machines n’ont pas de _hardware sprites_ donc pendant des années j’ai joué avec un _framerate_  15fps. Je me rappelle avoir éprouvé une admiration devant les jeux de la [NES](https://www.grospixels.com/site/nes.php) ou ce que je voyais dans les salles d’arcade avec tous ces _sprites_, parfois énormes, qui bougeaient en même temps… Les bornes d’arcade étaient difficiles à trouver. Quand je marchais dans les rues, je regardais toujours dans les bars pour voir s’il y en avait une. J’avais même l’oreille fine pour entendre le son des [CRT](https://fr.wikipedia.org/wiki/Tube_cathodique). Même si je ne la voyais pas je pouvais l’entendre. 

Pareil quand [DOOM](https://www.grospixels.com/site/Doom.php) est sorti, j’avais un [PC](https://www.grospixels.com/site/pc.php) 386 et ça ramait un truc de ouf. J’avais un ami qui avait un 486 SX et ça avait été la gifle de voir ce que l’expérience était avec du matos adéquat.

Plus tard, quand j’ai pu développer les compétences techniques, j’ai eu envie de revenir sur ces technologies pour comprendre la quête du _frame-per-second_.

###  Comment en êtes-vous venu à vous intéresser au CPS-1 ?



J’étais en vacances en Corse en 1992 et je cherchais les bornes d’arcade dans Bonifacio. Je devais avoir 14 ans et je me tapais toute la vieille ville et les remparts pour les trouver. Près du port je trouve une borne au fond d’un bar. [Street Fighter II](https://www.grospixels.com/site/street1.php) tournait dessus et ça a été une gifle monumentale. Il devait y avoir 10 personnes autour et ça a été un tournant. La machine n’avait que trois boutons (y'avait pas les pieds) et, bien sûr, les coups spéciaux n’étaient pas expliqués, mais ça reste un moment magique. J’ai énormément joué à ce jeu quand j’arrivais à le trouver (enfin tant que j’avais des pièces de [5 F](https://commons.wikimedia.org/wiki/File:5_French_francs_Semeuse_silver_1960_F340-4_reverse.jpg?uselang=fr) et 10 F). 

Plus tard, avec les compétences acquises dans mes études, d’autres jeux vidéos et les bouquins précédents, j’ai voulu comprendre comment ça marchait. En particulier, je voulais comprendre la différence entre une machine de salon comme la NES ou la SNES et une machine « industry-grade » qui permet autant de couleurs et surtout une taille de sprites aussi gigantesque.
 


### Qu’est-ce que ce système a de particulier ?



Ça a été un tournant pour [Capcom](https://www.capcom.com/). Ça prouve que les directeurs et le CEO ont su sentir le vent venir. J’entends parfois des programmeurs râler après la direction et les gens « pas techniques » mais c’est un exemple que sans ce type d'initiatives, la boîte aurait subi le même sort que Technos (NdM: Technos a fait faillite en 1996, leurs derniers jeux utilisaient le système Neo Geo de [SNK](https://fr.wikipedia.org/wiki/SNK_Corporation)).

C’est aussi un système d’une grande simplicité. Grosso-modo c’est trois arrière-plans et un plan pour les _sprites_. Plus j’étudiais le CPS-1 (je parle de ça dans l’épilogue du livre) et plus je me disais c'etait une machine simple et puissance mais pas du tout une silver bullet, surtout comparé à la [Neo-Geo](https://www.grospixels.com/site/neogeo.php). Peut-être que c’est là, finalement, sa force, le petit plus nécessaire a fait un bon jeu : ce n’étaient pas des gros processeurs comme les bornes de [Sega](https://www.sega.fr/) ou [Namco](https://fr.wikipedia.org/wiki/Namco), mais les _designers_ et les artistes de Capcom.

![CPS1 CPU](408px-CPS1_CPU.JPG)

### Que dire sur les CPS-2 et CPS-3 ?



Le CPS-2 ajoute de la RAM, un plus gros CPU (12MHz vs 10MHz), et un HSYNC interrupt…  mais c’est vraiment question lutte contre le piratage que c’est une machine impressionnante. Autant le CPS-1 s’était fait copier à outrance, autant le CPS-2 n’a été craqué que beaucoup plus tard, genre 15 ans après. La couche d’encryption que Capcom a mis sur le Z80 et le 68000 a été efficace et je leur tire mon chapeau. Il y avait tellement d’argent en jeu que probablement pas mal de gens doués (y'a qu’a voir ce que certains avaient réussi à faire en reprogrammant SF2 en accélérant la vitesse) ont essayé et ont échoué.

### Ces systèmes sont basés sur le [X68000 de Sharp](https://fr.wikipedia.org/wiki/Sharp_X68000) qui va bientôt ressortir en version [« mini »](https://mag.mo5.com/actu/231051/une-version-mini-aussi-pour-le-micro-ordinateur-japonais-x68000/) ? Qu’en pensez-vous ?


Une partie de l’attrait des X68000, c’est que ce sont des machines imposantes de par leur taille et leur poids. Rien que l’objet sur la table, avec son design "Manhattan", c’est magnifique. Le ressortir en plastique et à échelle 1:3, alors qu’il y a déjà des émulateurs disponibles, ça ne m’attire pas. Si ça avait été une reproduction 1:1 et que j’avais pu faire tourner Linux dessus ça aurait été un achat immédiat. Maintenant, il y a tellement de gens pour qui c’est une machine mythique que je ne doute pas que ça va être un succès.

![x68000](640px-Vcfe2007_img_5135.jpg)

### Les émulateurs CPS-1 sont-ils bons ?



J’ai essayé [XM6 Pro-68k](https://mijet.eludevisibility.org/XM6%20Pro-68k/XM6%20Pro-68k.html) (_NdM_ : malheureusement un logiciel non libre) et c’était tellement bon que je n’en ai pas testé d’autres. En plus il est pété de _features_ pour observer les _buffers_ et les couches intermédiaires. C’est au-delà de bon, c’est magnifique. J’ai commencé à m’intéresser à la [SNES](https://www.grospixels.com/site/snes.php) et j’espère que je trouverai un émulateur comme ça.

### Quels sont vos jeux préférés pour ces systèmes ?



À part les ports de Capcom, j’aime beaucoup R-Type. Bonanza Bros est aussi impressionnant, il a un côté innovant qui lui donne beaucoup de charmes (comme les _sprites_ pré-rendus en _raytracing_). Il a aussi des défauts de _clipping_ qui me font me mettre dans les bottes du développeur. Ça rend le dur labeur palpable.

Je trouve tous les jeux de la X68000 intéressants. Il y aurait de quoi écrire un livre juste en les faisant tourner dans [XM6 Pro-68k](https://mijet.eludevisibility.org/XM6%20Pro-68k/XM6%20Pro-68k.html) et en étudiant comment ils marchent. Le peu que j’ai vu sur les ports de Capcom montrait des trésors d’inventivité. C’est dommage que peu de personnes écrivent dessus. Il y a plein de gens très très doués, surtout dans la communauté française, bien plus compétents que moi d’ailleurs, mais qui n’arrivent pas à se motiver pour écrire des articles (vous savez qui vous êtes). C’est super dommage que ce savoir se perde un jour. Peut-être quand ces gens là arriveront à la retraite on verra des articles sortir. Peut-être que c’est juste une question de patience.

### Pourquoi créer un SDK en 2022 pour un si vieux système ?


Pour tester mes connaissances du _hardware_. Tu peux pas vraiment connaître le _hardware_ si tu ne connais pas le _software_ qui tourne dessus. Surtout le côté _bootstrap_. D’ailleurs, ça m’a servi à me détromper plusieurs fois. Et puis aussi parce que quand j’ai commencé à écrire le bouquin je me disais que ça devait exister et j’étais surpris que personne ne s’en soit chargé vu la popularité de la plateforme. Après, j’en profite pour dire que c’est encore un [_WIP_](https://fr.wikipedia.org/wiki/Work_in_progress), il manque des trucs comme les _backgrounds_ ou la musique. _Pull Requests welcome!_ (en clair : ya du taf').

### Que préférez-vous ? L’arcade à la maison ou l’arcade en arcade ?


Mes souvenirs des salles d’arcade, c’est des bars crado avec des gens qui craignaient un peu. Et aussi [10 F](https://fr.wikipedia.org/wiki/Pi%C3%A8ce_de_10_francs_Mathieu#/media/Fichier:10_francs_Mathieu_1987_F365-28_revers.png) en poche pour toute l’après-midi. De ce point de vue là, à la maison c’est bien mieux. Par contre tu joues seul. On peut pas tout avoir.


![home arcade](Arcade_bartop.jpg/800px-Arcade_bartop.jpg)

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?



En majorité Ubuntu. C’est facile à installer et j’ai jamais eu de problèmes sur mes Thinkpads. J’utilise encore Windows pour Cyberduck et Photoshop. C’est dommage qu’il n’y ait pas de client [SFTP](https://fr.wikipedia.org/wiki/SSH_File_Transfer_Protocol) potable sur Linux. J’ai essayé de configurer [KVM](https://fr.wikipedia.org/wiki/Kernel-based_Virtual_Machine) mais c’était un carnage et j’ai abandonné après plusieurs heures à rechercher des problèmes obscurs de [TPM](https://fr.wikipedia.org/wiki/Trusted_Platform_Module). Sinon j’utilise beaucoup [LaTeX](https://www.latex-project.org/) et [Evince](https://wiki.gnome.org/Apps/Evince). Je sais pas trop ce que ça veut dire « libres », j’ai peut-être répondu à côté de la plaque, désolé :( !

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?



J’utilise 99% du temps [CLion](https://www.jetbrains.com/clion/), [Golang](https://go.dev/) et [Intellij](https://www.jetbrains.com/idea/). Ce sont des merveilles. En logiciel libre…, [git](https://git-scm.com/) ?
 

### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?



Je n’ai jamais regardé ailleurs que Ubuntu, car j’ai toujours été très content du truc. Sauf pour les clients SFTP qui est très bugged (c.f: J’espère un port de [CyberDuck](https://cyberduck.io/)).
 

### Quelle question auriez-vous détesté qu’on vous pose ?



Vous n’aviez vraiment rien de mieux à faire de votre temps que d’écrire un livre entier sur une plateforme morte depuis 20 ans ?

### Quelle question auriez-vous adoré qu’on vous pose ?


Quels sont les outils utilisés pour écrire un livre. Quand j’ai commencé ça m’aurait économisé beaucoup de temps d’avoir une méthode et des exemples de projet. C’est aussi pour ça que je publie les [sources de mes bouquins](https://github.com/fabiensanglard).

### Ce sera peut être pour une autre dépêche consacrée à la documentation, car beaucoup de gens s'intéressent au sujet sur linuxfr.
