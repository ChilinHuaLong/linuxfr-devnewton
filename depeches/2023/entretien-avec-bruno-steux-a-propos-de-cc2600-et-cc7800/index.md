URL:     https://linuxfr.org/news/entretien-bruno-steux-a-propos-de-cc2600-et-cc7800
Title:   Entretien Bruno Steux à propos de cc2600 et cc7800
Authors: devnewton 🍺
         Benoît Sibaud
Date:    2023-11-08T18:11:10+01:00
License: CC By-SA
Tags:    
Score:   3


Bruno Steux développe des [compilateurs](https://github.com/steux) pour créer des jeux pour les consoles [Atari 2600](https://fr.wikipedia.org/wiki/Atari_2600) et [Atari 7800](https://fr.wikipedia.org/wiki/Atari_7800), cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.


![Atari 2600](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Atari2600a.JPG/640px-Atari2600a.JPG)

----

[cc2600](https://github.com/steux/cc2600)
[cc7800](https://github.com/steux/cc7800)
[Le dossier de grospixels sur l'Atari 2600](https://www.grospixels.com/site/vcs.php)
[atarimania](http://www.atarimania.com/)

----

# Partie 1 : Présentation

## Qui êtes-vous, quel est votre parcours et est-il lié  aux jeux vidéos?

Je suis Bruno Steux, 50 ans. Je travaille comme ingénieur dans la défense et je ne suis pas vraiment gamer. J’ai débuté l’informatique très tôt sur un TI 99/4A, puis j’ai enchaîné sur C64 puis Atari ST. J’ai passé mon adolescence pendant l’âge d’or du jeu d’arcade : des milliards d’heures sur Commando, Bomb Jack, Green Beret... C’est une période dont je suis resté très nostalgique. Aujourd’hui, je joue très rarement, sauf à Dr. Mario avec ma femme (on se fait des duels infernaux), et un peu avec mes Atari avec mon fils de 9 ans.

## Comment en êtes-vous venu à vous intéresser aux consoles Atari ?

J’ai appris l’assembleur du 6502 sur mon C64 étant jeune et ai gardé un souvenir impérissable des deux pages sur l’Atari 2600 dans le numéro spécial de l’ordinateur individuel de décembre 1982. C’était une console très chère à l’époque et je rêvais d’en avoir une. L’année dernière, j’ai lu le livre "Racing the beam" et ai commandé ma première console sur eBay, une pauvre 2600 SECAM bien pourrie. J’ai ensuite découvert l’univers [d’AtariAge](https://atariage.com/) et je me suis retrouvé scotché.

## Est-ce que vous participez vous même à la création de jeux ?

Pas vraiment, mais c’est dans les cartons. J’ai écrit un premier jeu disponible gratuitement pour Atari 2600 : [Paul’s Happybird](https://github.com/steux/happybird) (Paul est le prénom de mon fils de 9 ans) et un deuxième fini mais pas encore diffusé (Armor Ambush Reloaded). Pas eu le temps de faire l’annonce sur AtariAge... Une fois les projets cc2600 et cc7800 complètement terminés, je compte me mettre à la réalisation de jeux complets plus ambitieux.

# Partie 2: L’Atari 2600

## Qu’est-ce que cette console a de particulier ?

Pour moi 3 éléments uniques: son design - elle reste la plus belle console de tous les temps, avec ses switchs en métal et sa façade de faux bois, sa communauté active sur AtariAge, et son modèle de programmation tellement unique et plein de challenges.

## Comment fonctionne le processeur graphique et sonore (Television Interface Adaptor) ?

Ah, le TIA. Un processeur graphique de machine à Pong, franchement pas vraiment bien conçu même pour l’époque. 5 sprites (2 joueurs, la balle, et deux missiles) et un background de 40 pixels de résolution, et un paquet de registres à modifier à la volée à chaque ligne à afficher pour faire autre chose qu’un Pong... Avec un processeur lent, mais lent... 76 cycles processeurs par ligne. Un vrai challenge qui motive les meilleurs programmeurs depuis plus de 40 ans...

## L’Atari 2600 a connu plusieurs versions (VCS, CX2600, 2800, 2600 Jr, 2600+...) ? Les avez-vous testé?

Je suis l’heureux propriétaire d’une Darth Vader Secam, une Darth Vader PAL et une magnifique light six PAL fabriquée à Sunnyvale, sortie de grange anglaise et donnée comme morte, mais qui après nettoyage s’est révélée parfaitement vivante. La 2600 Jr ? Trop laide. Je pense acheter l’Atari 2600+, ne serait-ce que pour vérifier que les jeux que je développe tournent bien dessus (surtout en mode 7800).

![Atari 2600 noire](https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Atari-2600-Four-Switch-Black-Console-01.jpg/640px-Atari-2600-Four-Switch-Black-Console-01.jpg)


## Quelle est votre manette préférée ?

La manette d’origine de l’Atari 2600 est inimitable. Elle a une très mauvaise prise en main, un seul bouton, mais elle est tellement belle. De loin ma manette préférée, iconique. La manette de l’Atari 7800 est la pire jamais vue. Elle fait très mal très vite. Heureusement, on peut facilement utiliser des manettes de MegaDrive grâce un petit adaptateur facile à souder...

![Patrick secoue son stick](atari-2600-commercial-parker-brothers.jpg)


*NDM : oui c'est bien Patrick Bruel à la manette dans une [publicité](http://www.atarimania.com/videos/atari-2600-commercial-parker-brothers.mp4) avec des captures vidéos pas du tout truquées*.

## Les émulateurs Atari 2600 sont-ils bons ?

J’utilise [Stella](https://stella-emu.github.io/). Il est plus que bon: il est excellent. Il émule même le code ARM pour les cartouches à processeur additionnel, ou encore [l’AtariVox](https://www.atari7800forever.com/autovox.html) pour la synthèse vocale... Même la connexion Wifi de la [PlusCart](https://github.com/Al-Nafuur/United-Carts-of-Atari) est émulée... Le débugger intégré est tout à fait complet.

## Quels sont vos jeux commerciaux préférés sur cette console ?

Moon Patrol, Q\*Bert et Ms. Pacman sont les jeux auxquels j’ai le plus joué. Ils sont très bons, même pour 2023. Space Invaders ou encore Circus Atari (avec les paddles!) sont excellents aussi.

![Q-Bert](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Q%2Abert_in_Computerspielemuseum_Berlin.jpg/640px-Q%2Abert_in_Computerspielemuseum_Berlin.jpg?uselang=fr)


## Quels sont vos jeux "homebrew" préférés sur cette console ?

Lode Runner et Boulder Dash. Le premier est incroyable, même si le code tourne sur un ARM dans la cartouche (il faut quand même passer par le TIA pour l’affichage...). Le second n’utilise même pas d’ARM et est une incroyable réussite technique. J’ai acheté les deux cartouches sur AtariAge. Amoeba Jump est un petit jeu gratuit génial si vous ne connaissez pas.

# Partie 3 L’Atari 7800

![Atari 7800](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Atari-7800-wControl-Pad-L.jpg/640px-Atari-7800-wControl-Pad-L.jpg)


## Qu’est-ce que cette console a de particulier ?

Outre son chip Maria excellent, il se trouve que c’est une console qui est complètement compatible Atari 2600 ET qui a une sortie [Peritel](https://fr.wikipedia.org/wiki/P%C3%A9ritel) pour la version française. Elle est donc, contrairement à ses aïeules, très simple à brancher sur un téléviseur moderne...

## Est-elle juste une Atari 2600 boostée ?

C’est vraiment la descendante de la 2600, car elle est 100% compatible : elle utilise un 6502 et le TIA. Le format de cartouche est compatible aussi (8 broches en plus sur les côtés pour les cartouches de 7800). Mais la présence de Maria n’en fait pas qu’une 2600 boostée. On passe d’une console Pong à un équivalent NES ou Sega Master Systems, sensiblement supérieure à un C64 par exemple.

## Comment fonctionne le processeur graphique (MARIA) ?

Ce composant très simple et très low cost a été conçu par GCC (General Computer Corporation), les concepteurs du jeu d’arcade Ms. Pacman. C’est une puce très flexible dont l’architecture se rapproche de celle des jeux d’arcade de l’époque. Pas de notion de sprite ou de background, juste des listes d’objets à transférer par DMA (Direct Memory Access). On a donc une machine capable d’afficher des bitmaps ou des sprites sans limite de nombre ou de taille autre que celle imposée par son petit bus mémoire 8 bits. Une puce vraiment géniale mais qui impose au programmeur un modèle de programmation exotique, d’autant que le mapping mémoire impose d’entrelacer code et graphiques (les deux lignes successives d’un "sprite" doivent être séparées de 256 octets en mémoire. Sic.). Le tout uniquement en assembleur car le 6502 est allergique à tout langage de haut niveau. D’où une logithèque bien pauvre à la fin... Mais une machine excellente qui reste à exploiter en 2023 !

## Les émulateurs Atari 7800 sont-ils bons ?

Oui, [a7800](https://github.com/7800-devtools/a7800) est excellent, parfaitement "cycle accurate" et doté d’un excellent debugger intégré pour les programmeurs.

## Quels sont vos jeux commerciaux préférés sur cette console ?

Franchement? Je trouve les jeux commerciaux sur Atari 7800 globalement
mauvais, voire indécents (Jinks, Aces of Aces). J’ai été très décu par
les adaptations d’Ikari Warriors et de Double Dragon, considérés
pourtant comme des bons jeux sur Atari 7800. Commando est correct, mais
sans plus par rapport à la version C64 à laquelle je jouais petit. Les
jeux réalisés par GCC (les concepteurs de Maria) sont très corrects,
mais il s’agit pour la plupart d’adaptations de jeux d’arcades assez
simples (Dig Dug, Ms. Pacman).


## Quels sont vos jeux "homebrew" préférés sur cette console ?

Je n’ai joué à aucun jeu homebrew sur Atari 7800. C’est d’abord pour moi une plateforme de développement. J’ai vu sur Youtube l’adaptation de 1942 qui est vraiment très bonne, mais elle n’est pas diffusée sur cartouche. Rikki & Vikki a l’air aussi excellent, mais est aujourd’hui introuvable.

# Partie 4 : cc2600 et cc7800

## Pourquoi créer des compilateurs aujourd’hui pour de si vieux système ? Pourquoi le C ?

Pourquoi le C ? Parce que c’est le standard pour le développement système, a priori ce qu’il faut pour ce type de consoles. J’ai fait du C pendant 30 ans, donc naturellement quand j’ai découvert les Atari 2600 et 7800 et que j’ai vu que tout était programmé soit en assembleur, soit en basic, je me suis dit qu’il fallait faire quelque chose. Il existe bien déjà un paquet de compilateur C pour le 6502 dont l’excellent cc65, mais le 6502 est un processeur qui se prête très mal au langage C (nombre de registres très réduit et capacités d’indexation extrêmement limitées). Résultat: le code généré par cc65 est inutilisable (trop gros) sur les Atari. Sur la NES, pas de souci, ça passe, car le processeur graphique de la NES fait l’essentiel. Sur les Atari 2600 comme 7800, c’est le 6502 qui travaille et qui doit alimenter le TIA ou MARIA le plus vite possible. Le compilateur C doit donc générer un code proche d’un code assembleur écrit à la main, d’où cc2600 et cc7800. Ils ne sont pas aussi complets que cc65 mais génèrent le code assembleur 6502 qui convient pour les Atari.


## Quels ont été les difficultés pour créer cc2600 et cc7800 ?

Je n’ai pas vraiment rencontré de difficulté pour être franc. J’ai programmé cc6502 (le code commun à cc2600 et cc7800) avec grand plaisir en Rust, en utilisant la librarie Pest pour gérer la grammaire du C. Ca a été un régal. J’ai écrit de nombreux tests unitaires (une facilité intégrée à Rust) pour éviter de me perdre dans les bugs. Au bout d’une semaine, j’avais un compilateur opérationnel, puis je l’ai amélioré au fur et à mesure du développement de mes jeux ou exemples sur Atari 2600 puis 7800. Je me suis surtout servi de ce projet personnel pour apprendre le Rust, et je crois que l’objectif est atteint.


## Quels conseils donneriez-vous à quelqu’un qui veut se lancer dans le développement de jeux avec ces compilateurs ?

Ces compilateurs peuvent faire gagner beaucoup de temps comparé à l’utilisation de l’assembleur, mais la spécificité des architectures des Atari 2600 et 7800 imposent de d’abord bien les maîtriser. On ne peut pas s’affranchir non plus d’apprendre l’assembleur du 6502, car cc6502 est un compilateur C incomplet orienté 6502 (il permet notamment d’utiliser directement les registres X et Y du 6502) et que tout débogage se fera en assembleur…


## Quels sont les outils pour créer/préparer des graphismes, musiques et sons pour ces consoles ?

J’ai écrit également en Rust [sprites7800 et tiles7800](https://github.com/steux/tools7800) qui génèrent le code C des graphismes à partir d’images PNG et d’une description en YAML des sprites et des tiles à générer. Pour la musique, il existe l’excellent RMTracker sur Atari 7800 qui permet de réaliser de belles musiques de fond pour le chip POKEY, et que j’ai intégré à cc7800. Pour les musiques à base de TIA (pour 2600 comme 7800), je fournis un exemple d’intégration d’un ensemble de sons réalisés par l’auteur de 7800Basic. Il existe aussi un TIA tracker, mais je ne l’ai pas encore intégré. J’essaie de développer au fur et à mesure de nouveaux “headers” pour cc2600 et cc7800 pour faciliter le travail des développeurs.


## Est-il possible de créer ses propres cartouches ?

C’est très simple sur la 2600 comme sur la 7800. On trouve les schémas de PCBs tout fait sur internet avec les fichiers pour réaliser les cartouches en impression 3D. Il n’est pas très complexe non plus de réaliser des cartouches intégrant un micro-contrôleur type STM32F (en se basant par exemple sur le projet UnoCart), qui peut non seulement émuler l’EPROM mais exécuter du code spécifique (accélérateur matériel) ou encore assurer une connexion Wifi via un ESP32.


## Comment s’accommoder des faibles ressources (peu de RAM, CPU sans virgule flottante…) ?

Vous parlez des 128 octets de RAM à partager entre variables et pile sur l’Atari 2600 ? Ca fait partie du jeu ! Je trouve que finalement on s’en accomode facilement. Le plus contraignant est cette sacré limitation de décalage bit par bit sur le 6502… Et l’absence de multiplieur matériel… Le plus souvent, on s’en tire en précalculant tout et en utilisant des tables, la taille de la ROM et donc du code étant comparativement très peu limitée.


# Partie 5: Pour finir

## Vous aimeriez vivre du développement de vos logiciels libres?

Idéalement oui, mais je ne vois pas vraiment comment pour être franc… Pour l’instant, ça m’a coûté beaucoup de temps et pas rapporté un kopek, mais sait-on jamais. La communauté Atari, aussi dynamique soit-elle, reste réduite. J’espère que l’Atari 2600+ sera un succès. Bien que bourrée de défauts, ça ma paraît être la proposition la plus sérieuse depuis longtemps de relance d’une activité par Atari.


## Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?

Je développe tout sur mon PC sous Ubuntu 22.04. Je programme essentiellement en Rust en utilisant Neovim comme éditeur. J’utilise aussi GIMP pour tout ce qui est graphisme. Avec ces outils, je couvre 99% de mes besoins.


## Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?

Pareil, au niveau professionel : développement en Rust sous Ubuntu 22.04. J’utilise Firefox comme navigateur. Pour la partie bureautique, j’ai un second PC professionnel sous Windows avec Word et compagnie. J’avoue avoir toujours du mal avec LibreOffice…


## Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?

Je devrais répondre ArchLinux si j’étais un vrai un geek, mais franchement Ubuntu me satisfait pleinement. C’est simple et ça marche. Mes logiciels libres préférés ? Rust, LLVM, Vim et Neovim, GIMP. Et KiCad pour le design de circuits. J’utilise aussi parfois Blender et Audacity. Blender est particulièrement impressionnant comme projet open source.


## Quelle question auriez-vous adoré qu’on vous pose ?

Comment expliquez vous les dizaines de milliers de like que vous obtenez pour chacune de vos publications sur AtariAge ? Humm, mon record de like est à 13. Ca porte bonheur non ?


## Quelle question auriez-vous détesté qu’on vous pose ?

Vous n’avez pas mieux à faire que de développer pour une plateforme complètement oubliée depuis plus de 30 ans ?
