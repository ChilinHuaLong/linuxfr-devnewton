URL:     https://linuxfr.org/news/entretien-avec-stephane-d-a-propos-du-sgdk
Title:   Entretien avec Stephane-D à propos du SGDK
Authors: Dave Newton 🍺
         Pierre Jarillon, gUI, Ysabeau, bobble bubble, Xavier Claude, Benoît Sibaud et Florent Zara
Date:    2023-02-13T09:18:02+01:00
License: CC By-SA
Tags:    jeu_vidéo et retrogaming
Score:   49


Stephane-D développe [un SDK pour créer des jeux pour la console Megadrive](https://www.patreon.com/SGDK), cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.



![Console Megadrive, source Wikipéadia, © Bill Bertram (Pixel8) 2011](320px-Sega_Mega_Drive_PAL.jpg)



[Code du SDK sous licence MIT](https://github.com/Stephane-D/SGDK)
[Un article présentant le hardware de la Megadrive](https://www.copetti.org/writings/consoles/mega-drive-genesis/)
[Un podcast publié pour les 30 ans de la console](https://mag.mo5.com/150642/les-podcasts-de-mo5-com-42-la-mega-drive-a-30-ans/)
[Un autre podcast sur l’histoire de SEGA](https://cpu.dascritch.net/post/2020/06/11/Ex0139-Sega%2C-game-over-sur-console)
[Un site consacré à SEGA et ses consoles](https://www.sega-16.com/)
[Z-Team, une équipe qui porte des jeux micros sur Megadrive](https://z-team.itch.io/)

----


----

## Présentation


### Bonjour, qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?


Bonjour,



Mon nom est Stéphane Dallongeville, j’ai 43 ans et je suis développeur de métier. Étrangement, j’ai toujours travaillé dans le domaine de la santé, pas forcément par choix, mais plutôt par le hasard de la vie et surement parce que c’est un domaine où les besoins sont assez techniques et que c’est ce qui m’intéresse en général 🙂.
Contrairement à bon nombre de personnes de ma génération, j’ai découvert les jeux vidéos avec les bornes d’arcade vers l’age de 7-8 ans. Il y avait une salle de jeux d’arcade (le Flipper's house de Compiègne pour celles et ceux qui ont connu) juste à côté de mon école et, avec mon frère, on avait pris l’habitude d’y passer avant et après l’école (avec pas mal d’arrivées en retard à cause de ça) et d’y passer une bonne partie de nos week-ends également. Je me souviens qu’on était vraiment accro, on venait juste pour regarder les gens jouer, car on n’avait pas ou très peu d’argent à dépenser. Du coup, on a découvert pas mal de classiques en arcade (Pole Position, Pac Man, Double Dragon, WonderBoy, Street Fighter 2, Final fight…) avant même d’avoir une console. D’ailleurs notre première console (en 1990) fut une… [GX-4000](https://www.gx4000.net/) 😅.
Là encore, ce n’était pas forcément par choix, mais simplement parce que c’était la seule en stock (mes parents achetaient beaucoup par correspondance), il faut admettre qu’elle ne nous a pas laissé de souvenirs impérissables… Mais par la suite, on s’est bien rattrapé avec la NES d’abord, puis la Megadrive, la SNES, etc.
J’ai passé beaucoup, beaucoup d’heures à jouer étant jeune, mais à partir de 16 ans, j’ai commencé à m’intéresser davantage au PC et à la programmation, j’ai alors délaissé un peu les consoles.
Il faut dire que la génération 32 bit ne m’a jamais vraiment attiré, déjà parce que le prix des consoles devenait indécent (_NDM : peut-être un lapsus faisant référence à [Descent](https://www.mobygames.com/game/Descent) ?_ ) mais surtout parce que le passage à la 3D s’est fait dans la douleur… Même si techniquement, c’était certes une révolution, visuellement, c’était moche et pendant ce temps-là sur PC, on commençait à avoir de la 3D filtrée bien plus flatteuse.



### Comment en êtes-vous venu à vous intéresser à la megadrive ?



J’ai toujours été plus Sega que Nintendo, surement parce que j’ai un frère jumeau et, quand on était jeune, il fallait se différencier l’un de l’autre, comme il était plutôt Nintendo alors, j’étais Sega, avec le recul, je pense que ça correspondait aussi un peu à nos différences de caractère 😄.



La génération des consoles 16 bits est indéniablement celle qui m’a le plus marqué et, donc, la Mega Drive est un peu ma machine de cœur même si j’ai passé au moins autant de temps à jouer sur SNES.



## Parlons hard


### Qu’est-ce que cette console a de particulier ?


Déjà, c’est une console de la génération 16 bits et donc on est encore sur des machines pensées pour la 2D, je pense qu’on aurait pu aller beaucoup plus loin sur les jeux 2D si l’arrivée de la Playstation n’avait pas « imposé » un saut précoce à la 3D. Mais du coup « faute de mieux » la génération 16 bits représentent un peu la quintessence des jeux 2D dans un sens.
Ensuite, par rapport à cette génération 16 bit et surtout par rapport à sa concurrente directe, la SNES, la Mega Drive est beaucoup plus simple à programmer, et, disons-le offre plus de liberté au développeur contrairement à ce qu’on pourrait penser. Son processeur central - le 68000 - qui était un peu « la rolls » des processeurs 16 bits de l’époque, est en grande partie responsable de cet état de fait, surtout en comparaison du 65C816 qui équipe la SNES et qui est à l’extrême opposé. Mais le reste de l’architecture n’est pas en reste. La SNES est plus moderne assurément (elle est sortie quand même 2 ans après), elle affiche plus de couleurs avec beaucoup d’effets graphiques hardware et un son à base de samples, mais l’ensemble est assez contraint et il est difficile de sortir des frontières dessinées par les capacités hardware de la machine. La Mega Drive c’est un peu l’opposé, on a peu d’effets câblés, c’est assez root avec du son FM et des capacités PCM réduites au minimum syndical. Par contre, on a beaucoup de flexibilité et de puissante brute pour dépasser ces frontières et c’est ça qui est plaisant avec cette machine.



### Comment fonctionne le processeur graphique (VDP) ?



C’est un processeur graphique optimisé pour l’affichage 2D, ça reprend le même principe qu’on avait sur les consoles 8 bits : un arrière-plan scrollable et des sprites qu’on affiche par-dessus.
L’affichage est composé exclusivement de blocs de 8 × 8 pixels en 4 bpp (16 couleurs) qu’on appelle « tuile », la mémoire vidéo (VRAM) va donc contenir un set de tuiles et des tables permettant de mapper ces tuiles sur les plans et sur les sprites (des tables d’index en gros).
Par rapport à la génération précédente, la Mega Drive apporte un 2ᵉ plan scrollable pour ajouter plus de profondeur et de complexité dans les décors ainsi que des capacités de sprites gonflées (taille variable de 8 × 8 à 32 × 32 et jusqu’à 80 sprites maximum).
Le VDP apporte aussi quelques fonctionnalités hardware pour faire des parallaxes facilement ainsi qu’un scrolling vertical assez avancé qui permettait de scroller des colonnes de 16 pixels individuellement (c’est grâce à lui qu’on pouvait réaliser des effets de rotations avec des angles restreints par exemple). Une fonction majeure de cette génération était l’apport du DMA qui permettait de faire des transferts rapides vers la mémoire vidéo. Par contre, la faiblesse majeure du VDP c’est sans conteste son faible nombre de palettes : seulement 4 palettes de 16 couleurs sur une profondeur de 512 couleurs (RGB333), c’est vraiment peu.
La SNES en a 4 fois plus (8 palettes pour les arrières plans + 8 palettes pour les sprites) et la PC Engine pourtant sortie 1 an avant en avait pas moins de 16 + 16 palettes. C’est d’autant plus dommage qu’à la base le VDP a été conçu pour accueillir 4 + 4 palettes en RGB444 mais pour des raisons de coût ça a été tronqué à 4 palettes partagées en RGB333.


_NDM: pour aller plus loin voir le site [Rasterscroll](https://rasterscroll.com/mdgraphics/)._



### Comment fonctionnent les puces sonores (Yamaha YM2612, Texas Instruments SN76489) ?



Le SN76489 est vraiment un générateur audio assez basique que l’on retrouve sur beaucoup de machine 8 bit. Il est d’ailleurs présent sur la Master System…
C’est un générateur d’onde carré sur 3 voies + 1 voie de bruit blanc, les capacités audio de ce chip sont, disons-le, très limitées…
Le YM2612 est un synthétiseur FM sur 6 voies, c’est le même type de synthétiseur qu’on trouvait dans certains synthétiseurs des années 80 (comme le Yamaha DX 11). Le principe de la synthèse FM est assez complexe, chaque voie est composée plusieurs opérateurs (4 sur le YM2612) qui sont connectés ensemble selon plusieurs configurations possibles (8 possibles sur le YM2612). À la base, chaque opérateur produit une onde sinusoïdale à une certaine fréquence et une enveloppe de type ADSR par-dessus. Ensuite, selon la configuration des opérateurs, certains opérateurs vont servir à « moduler » l’onde sonore de l’opérateur suivant ou simplement produire le son de sortie.
Prenons l’exemple de la configuration #0, dans cette configuration, les 4 opérateurs sont connectés en série : 0 --- 1 --- 2 --- 3
On a donc l’opérateur 0 qui va moduler le son de l’opérateur 1, qui va lui-même moduler le son de l’opérateur 2, qui va lui-même moduler le son de l’opérateur 3 qui va lui sortir le son final de la voie. Cette configuration permet de produire des ondes très complexes.
À l’inverse, la configuration #7 positionne les 4 opérateurs en parallèle :



    0
    1
    2
    3



La sortie des 4 opérateurs est simplement mixée ensemble pour produire le son final de la voie.
Une particularité du YM2612 est que la 6ᵉ voie peut être configurée en simple DAC pour pouvoir produire des samples (voix digitalisées), c’est grâce à ça que la Mega Drive peut produire du son digitalisé.
La synthèse FM reste un procédé sonore complexe et difficile à maîtriser qui est capable du meilleur comme du pire. Beaucoup de développeurs occidentaux se sont cassés les dents dessus (nos tympans en ont fait les frais) mais dans de bonnes mains ça pouvait produire du son extrêmement riche et de meilleure qualité que les sampleurs de l’époque. De par le principe de fonctionnement des chips FM, l’onde de sortie est générée à haute fréquence, sur le YM2612, on est à 53 KHz par exemple.



### Qu’est-ce que le Blast Processing ? Est-ce juste une blague du service marketing de Sega ?



Oui et non 🙂 Il n’y a donc pas de techno « Blast Processing » à proprement parler. À la base ça vient d’un trick trouvé par un développeur interne : il a découvert qu’on pouvait produire un pseudo-mode bitmap en transférant en continu en CRAM (palette) via le DMA. Il a utilisé le mot « blast » pour décrire ce procédé de transfert en continu via le DMA… et en effet le marketing a repris ce terme pour en faire le fameux Blast Processing, car ils trouvaient que ça sonnait bien !



_NDM : le marketing de Sega et de ses partenaires était connu pour être [agressif ](https://www.sega-dreamcast-info-games-preservation.com/pub-megadrive-aventure-numerisation), voire carrément [lourd](https://www.sega-16.com/2016/08/sex-sega-partners-in-print/)._



### Que dire sur le megacd ?



Je pense que le MegaCD est assez sous-estimé, ou plutôt sa ludothèque. Il y a beaucoup de très bons jeux sortis sur MegaCD.
Par contre, je pense sincèrement que le MegaCD aurait eu bien plus de succès si Sega s’était contenté d’en faire juste un « gros support de stockage » (comme sur la PCE) plutôt qu’en faire un add-on si couteux avec des fonctions de pseudos mode 7 uniquement là pour faire face à la SNES. À l’époque à plus de 2000Fr je n’ai jamais *regardé* le Mega-CD, il était hors course à un prix si prohibitif et le positionnement marketing sur le FMV était aussi une erreur, je pense. La Mega Drive en avait déjà pas mal dans le ventre et un simple gros stockage lui aurait permis de s’exprimer pleinement.



![MegaCD](MegaCD.png)



### Que dire sur la 32x ?



Là pour le coup c’est assez difficile d’en dire du bien si on considère la ludothèque de la machine… à peine 50 jeux dont une dizaine qui en vaille réellement la peine 😕.



Le 32X était une vaine tentative de prolonger artificiellement la vie de la Mega Drive mais le timing n’était pas le bon avec l’arrivée de la Saturn et de la Playstation.
Je pense que le 32X aurait peut-être pu avoir son marché comme « 32 bits abordable » en console standalone à cartouche. Là où la Saturn et la Playstation s’affichaient à 3000 Fr ou plus, le 32X aurait pu séduire les moins fortunés en se positionnant entre 1000 Fr et 1500 Fr, surtout qu’en puissance brute le 32X est plutôt capable !



![32x](Sega-Genesis-Model2-32X.jpg)



### La megadrive a connu plusieurs versions (model 2, CDX, Nomad, Teradrive, Wondermega, LaserActive…) ? Les avez-vous testés ?



Pas tous ! J’ai eu une MD2 que j’ai revendue, mais je possède encore une Nomad et un WonderMega (le premier).
J’ai aussi un TeraDrive, mais je ne l’ai pas encore utilisé (sic).
Personnellement, j’adore le design du WonderMega 🙂.



![Wondermega](Console-wondermega.jpg)



### La megadrive a récemment eu deux versions « mini » ? Qu’en pensez-vous ?



J’ai la première que j’aime beaucoup, j’ai dû la payer 50€ ou 60€ et pour le prix payé, je trouve le contenu (en termes de jeux) très correct.
Je n’ai pas pu tester la 2ᵉ version, mais j’en ai entendu pas mal de bien.
Après ce que je trouve dommage, c’est que ces deux versions mini ont été précédées de *beaucoup* de modèles AtGame dont la qualité était vraiment discutable…


![Megadrive mini](Sega_Mega_Drive_Mini.jpg)



### Quelle est votre manette préférée ?



Je n’ai pas vraiment de manette préférée, mais j’aime beaucoup la manette 6 boutons de la Mega Drive et celle de la Saturn.



![manette six boutons](Sega-Genesis-6But-Cont.jpg)



## Parlons soft


### Les émulateurs megadrive sont-ils bons ?


Ce n’était pas forcément le cas par le passé (dont mon propre émulateur Gens \^\^) mais aujourd’hui oui, ils existent plusieurs émulateurs de très bonne qualité (Genesis Plus, Exodus, BlastEm…).
Si je devais en retenir qu’un, je choisirais [BlastEm](https://www.retrodev.com/blastem/) qui est à la fois rapide et avec un très haut niveau de précision sur l’émulation.



### Quels sont vos jeux commerciaux préférés sur cette console ?



Difficile à dire mais là comme ça, ceux-là me viennent en tête : Street of Rage 2, Sonic 2, Gunstar Heroes, Ranger-X, Mickey Mania, Landstalker, Aladdin, Yuyu Hakusho.



![Gunstar Heroes](GunstarHeroes_SevenForce.gif)



### Quels sont vos jeux _homebrew_ préférés sur cette console ?



Pas d’ordre particulier là encore : Demons of Asteborg, Tanzer, Paprium, Xeno Crisis, Cave story, Omega Blast et j’en oublie c’est certain.



![Tanzer](599365-tanzer-screenshot.png)



### Pourquoi créer un SDK aujourd’hui pour un si vieux système ?



À la base, j’ai créé ce SDK (vraiment minimaliste au départ) pour moi, car il n’existait pas de bons outils pour développer sur cette machine (ça remonte à 2006 donc ça date !).
Je voulais construire des petites roms de test afin de perfectionner mon émulateur (Gens). Par la suite, c’est devenu un hobby parce que ça m’amusait vraiment de développer pour cette machine et j’ai commencé à ajouter de nouvelles fonctions, c’est aussi à ce moment-là que j’ai commencé à le distribuer en me disant que ça pourrait peut-être servir à d’autres. Les retours ont tellement été positifs que ça m’a poussé à continuer le développement, ce qui a abouti à SGDK 🙂.



### Est-ce que vous participez vous-même à la création de jeux ?



Oui ça m’arrive, j’ai participé notamment au développement de Demons of Asteborg (j’ai assisté l’équipe sous contrat) et actuellement, je travaille sur un autre jeu, mais le développement de SGDK reste mon activité principale, on va dire ! Je ne désespère pas de travailler sur mon propre jeu un de ces jours \^\^.



![Demons of Asteborg](swamp_and_people-1024x724.png)



### Quels ont été les difficultés pour créer SGDK ?



Le plus compliqué est assurément le développement des drivers son et le développement du driver XGM en particulier qui est le driver son principal de SGDK.



C’est compliqué, car ça nécessite de programmer sur le second CPU (Z80) entièrement en assembleur et avec des possibilités de debug très limitées comparé au CPU principal (le 68000). L’autre complexité, c’est que le driver son a besoin de gérer le stream PCM (il y a 0 assistance hardware pour ça) sans interruption en même temps que la gestion de la partie FM et PSG.
Je travaille actuellement sur une version 2 du driver XGM afin de réduire sensiblement la taille des _tracks_ (c’est un point faible du driver XGM actuel) en plus d’ajouter certaines fonctionnalités et je peux dire que ça me prend un temps considérable !



Sur les autres aspects, le développement de SGDK est plutôt « facile », seul le développement des outils annexes (compilateur de ressources par exemple) peut être assez ennuyant par moment…



### Vous aimeriez vivre du développement de ce logiciel libre?



En effet, j’adore ce que je fais avec SGDK et si je pouvais en vivre ça serait génial 🙂.



Mais je pense que c’est assez illusoire, car ça reste un marché de niche.



### Est-ce que SGDK gère les manettes à 6 boutons de la megadrive, les multitaps et les pistolets (Menacer Gun) ?



Pour tout ça oui, la gestion du multipad et des pistolets a d’ailleurs été une contribution extérieure 🙂.



Les pistolets sont supportés par SGDK mais ne fonctionnent pas avec les TVs modernes. Il me semble qu’il existe des kits pour les faire fonctionner sur des TVs modernes, mais honnêtement, je ne suis jamais vraiment allé voir ça… Sinon sur émulateur la souris permet de remplacer le pistolet.



![Sega Menacer](Sega_Menacer_cropped.jpg)



### Est-ce que SGDK gère les modems ?



Non, mais il existe un module [MegaWifi](https://gitlab.com/doragasu/mw) qui permet donc l’exploitation d’une cartouche spéciale avec un module Wifi (là encore c’est une contribution extérieure).



![MegaWifi](EgmwOITXgAMoi2q-1024x907.jpg)



### Est-ce que SGDK gère d’autres périphériques de la megadrive (souris, Miracle Keyboard System, Aura Interactor, Sega Activator…) ?



La souris uniquement, les autres périphériques étant vraiment anecdotiques (le Sega Activator n’est jamais sorti, il me semble ?).



_NDM: peut-être au [Brésil](https://www.sega-brasil.com.br/Tectoy/Activator_para_Mega_Drive) ?_



![Sega Activator](1280px-Activator_Caixa_Tras_MD_01.jpg)



### Quels conseils donneriez-vous à quelqu’un qui veut se lancer dans le développement megadrive ?



De ne pas vouloir aller trop vite \^\^. Il faut savoir que SGDK est un SDK en langage C, si vous ne connaissez ni le langage C ni l’architecture de la Mega Drive et que vous souhaitez vous lancer dans le développement megadrive avec SGDK alors je vous conseille très fortement de vous familiariser d’abord avec le langage C et la programmation en général.
Apprendre les deux en même temps, c’est vraiment très (trop) compliqué. 95% des requêtes d’aide / support / bugs que je peux avoir sur le serveur Discord de SGDK concerne d’abord des questions ou des erreurs de programmation plus que SGDK en lui-même. Ensuite, une fois qu’on est plutôt à l’aise avec le C, c’est toujours un gros plus de savoir comment fonctionne la machine _under the hood_. SGDK abstrait pas mal de choses, mais ça évitera tout de même des belles prises de tête sur la compréhension de certains bugs.
Enfin et surtout, bien lire le fichier README de SGDK qui donne une foule d’informations importantes sur l’utilisation de SGDK en général.
Après, je dirais que lorsqu’on a la motivation, on arrive toujours à ses fins 😉.



### Quels sont les outils pour créer/préparer des graphismes utilisables par SGDK ?



N’importe quel outil capable de gérer des PNG en couleurs indexées peut faire l’affaire, car c’est ce que SGDK exploite en général.
Malgré tout certains outils sont plus appropriés que d’autres, car on y manipule plus facilement les palettes, personnellement je conseille ceux-là :



- Asesprite (_NDM: un [fork libre](https://libresprite.github.io) existe_) ;
- Graphics gale ;
- Pro Motion ;
- [Grafx2](https://gitlab.com/GrafX2/grafX2) ;
- [Palette quantizer](https://rilden.github.io/tiledpalettequant/) (un très bon outil pour convertir les graphismes).



### Quels sont les outils pour créer/préparer des musiques et des sons utilisable par SGDK ?



Tout ce qui peut exporter au format [VGM Mega Drive](https://vgmrips.net/wiki/VGM_Specification) pour les musiques et des outils pour la retouche de samples.



- Deflemask (tracker)
- [Furnace](https://github.com/tildearrow/furnace) (tracker)
- Wavosaur ou Audacity (éditeurs WAV)



À noter que certains préfèreront développer leurs propres outils 😉.



### Est-il possible de créer ses propres cartouches ?



Oui bien sûr ! Aujourd’hui il existe plusieurs éditeurs qui peuvent produire des cartouches Mega Drive ([Broke studio](https://www.brokestudio.fr/) ou [MegaCats Studios](https://megacatstudios.com/) pour ne citer qu’eux).



### On a vu récemment des démos impressionnantes (portages de [Star Fox](https://mag.mo5.com/236549/star-fox-decolle-de-nouveau-sur-mega-drive/) ou [F-Zero](https://mag.mo5.com/66493/f-zero-aussi-porte-sur-mega-drive/)), comment est-ce possible ? Quelles sont vos astuces ou optimisations préférées ?



C’est un peu ce que je disais tout au début en parlant de la Mega Drive, son architecture laisse pas mal de libertés.



Déjà son CPU central - le [68000](https://fr.wikipedia.org/wiki/Motorola_68000) - a de la ressource ! Quand on l’exploite vraiment bien, on peut en sortir pas mal de choses (je parle relativement pour cette époque) 🙂.



Ensuite un autre avantage de la MD comparé à pas mal de ses concurrentes, c’est que les pixels des tuiles sont stockés en [chunky](https://en.wikipedia.org/wiki/Packed_pixel) et non en [planar](https://en.wikipedia.org/wiki/Planar_(computer_graphics)) ce qui permet de faire du rendu software plus facilement.
Des astuces que j’aime bien, c’est de jouer avec le registre d’incrément automatique sur l’adresse VRAM (après une écriture en VRAM, l’adresse destination est automatiquement incrémentée selon la valeur d’un registre) pour faciliter et accélérer la conversion des données graphiques (par exemple pour simuler un [frame buffer](https://en.wikipedia.org/wiki/Framebuffer) plus ou moins linéaire).
Il y a aussi beaucoup d’astuces et d’effet possibles en jouant avec les capacités de scrolling ou en reprogrammant le VDP pendant l’affichage (modification des couleurs, du scrolling vertical, des sprites). Là encore, le VDP se montre assez flexible sur ce point, ce qui ouvre les possibilités (multiplexing sur les sprites par exemple).


### Comment s’accommoder des faibles ressources (peu de RAM, CPU sans virgule flottante…) ?



Avec des astuces et comme on le faisait à l’époque :) La « faible RAM » n’est en réalité pas un vrai problème, car 64 Kio de RAM est en général bien suffisant, il faut bien voir qu’on a un accès direct à toute la ROM dont la taille est bien plus importante, la RAM va juste servir à stocker les données dynamiques. Aussi, aujourd’hui, on peut dépasser allègrement la barrière des 4 Mio pour la ROM ce qui là encore offre plus de possibilités et permet d’exploiter la machine à son plein potentiel. Il faut bien voir qu’à l’époque la taille de la ROM (donc le coût de production découlait directement) était vraiment une contrainte très forte sur le développement des jeux. Pour la virgule flottante, on utilise à la place une virgule fixe, c’était quelque chose d’assez courant à cette époque où les unités flottantes n’étaient pas encore répandues.
Quand on développe sur ce genre de machine, il faut quand même avoir une certaine connaissance de comment fonctionne un ordinateur en général pour comprendre ce qui consomme ou pas beaucoup de ressources.
 


## Libre à vous


### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?


J’utilise quasiment exclusivement des logiciels libres, tellement qui serait difficile de tous les énumérer, mais entre autres Firefox, Eclipse, Code::Blocks, VLC, LibreSprite, GCC et la suite GNU en général…
Étrangement par contre, je suis sous Windows, probablement parce que j’utilise beaucoup les émulateurs et qu’il faut admettre qu’on a plus de choix ou au moins plus de facilité de ce côté sous l’OS de Microsoft.
Mais vu que je ne joue plus aujourd’hui, il est assez probable que je repasse à Linux.



### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?



Au niveau professionnel, j’utilise certains des outils dont j’ai l’habitude comme Eclipse, mais principalement, on fonctionne sous Windows et Linux.
En termes de technos, on utilise beaucoup de choses venant du monde libre comme tout le monde, je pense (node, angular, react.js, etc.) 🙂
J’utilise aussi beaucoup Java pour le développement, professionnellement comme personnellement, mais depuis la récupération par Oracle, on ne peut plus parler de logiciel libre, je pense (_NDM : les distributions Linux et la plupart des entreprises utilisent aujourd’hui [Openjdk](https://openjdk.org/), la version libre de Java plutôt qu’Oracle Java qui a de gros morceaux privateurs dedans_).



### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?



Je n’utilise pas beaucoup Linux, mais j’aime les distributions plutôt légères comme [Lubuntu](https://lubuntu.me/) ou [Mint](https://linuxmint.com/).
Je n’ai pas vraiment de logiciels libres préférés, mais j’apprécie beaucoup Firefox même si ce n’est pas le navigateur le plus performant.



### Quelle question auriez-vous adoré qu’on vous pose ?



Pourquoi la Mega Drive et pas la Super Nintendo ? Mais dans un sens la question a été posée (pourquoi la Mega Drive ?) 🙂.



### Quelle question auriez-vous détesté qu’on vous pose ?



Je n’en vois aucune vraiment \^\^.
