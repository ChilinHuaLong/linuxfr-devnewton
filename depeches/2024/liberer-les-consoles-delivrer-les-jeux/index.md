
# Bring the fun back in Free and open source software community   : 



## Jeux libres

Interview Amiga Arcade Machine  :
https://www.amigaclub.be/projects/arcade
Le club belges  des utilisateurs  d'amiga a créer une borne d'arcade amiga , via la réutilisation d'une marchine Apple,
qui a été formaté avec une distribution linux pour le retrogaming : Batocera.linux  et ils ont créer les éléments de hardware qui vont avec pour avoir une look and feel d'une borne d'arcade  :
avant ils utilisaient un autre projet libre qui est la pandora box  (ce serait intéressant pour ceux qui sont dans la communauté pandora box de connaitre le type de fun et le niveau de fun chez eux )


question d'interview pour les utilisateurs d'amiga club  ayant créer l amiga arcade machine  https://www.amigaclub.be/projects/arcade

### 1.0 clone de logiciel propriétaire en libre

        https://osgameclones.com/   jeux cloné , inspiré de jeux propriétaire 

### 1.1 autre jeux libre spécifique

        https://itch.io/games/tag-open-source  

### 1.2 jeux totalement libre (asset et moteurs)

### 1.3 jeux libre  avec culture du libre (ex: mascotte de tux / gnu / freebsd etc .. comme supertuxkart)

        supertux advance https://kelvinshadewing.itch.io/supertux-advance
        tux : https://luckeyproductions.itch.io/tux
        tux revenge : https://migdyn.itch.io/tux
        tux pusher and friends : https://pushergames.itch.io/tuxpusher
        tux 3d : https://kravataf.itch.io/tux3d
        tux dodger : https://metalx1000.itch.io/tux-dodger
        tux scape : https://pushergames.itch.io/tuxscape
        linux elitist game : https://pushergames.itch.io/tuxscape
        tuxocide : https://pushergames.itch.io/tuxocide
        supertux classic : https://www.supertux.org/
        freedroid rpg : https://www.freedroid.org/

## 2 Inutlitaires libres
Bonjour Nal,
Nous t'écrivons aujourd'hui pour te parler de logiciels libres qui ne servent à rien. Dans un monde où l'injonction à être "efficace" est de plus en plus forte, il nous semblait important de se détendre un peu en parlant de logiciels qui ne servent strictement à rien.

## petit philosophie rapide de l'inutile
Comme le dit le dicto : inutile donc indispensable
https://www.rtbf.be/article/simon-brunfaut-dans-cette-epoque-dominee-par-l-utilitarisme-l-inutile-est-plus-que-jamais-essentiel-11463845

Les grands classiques en ligne de commande
https://hamvocke.com/blog/commandline-fun/

### Ascii art et ligne de commande 

#### cowsay
cowsay est un outil en ligne de commande qui permet de faire parler une vache en asciiart:
`

$ cowsay Bonjour Nal


< Bonjour Nal >

    \   ^__^
     \  (oo)\_______
        (__)\       )\/\
            ||----w |
            ||     ||
`


#### vi
VI est un éditeur de texte fait pour rendre la saisie, la sélection et la manipulation de texte le plus inutilement complexe possible en appliquant tous les antipatterns d'UX/UI possibles.



### Les logiciels punitifs (de rappel ? )

sl
Ce logiciel punit les dyslexiques ou les utilisateurs peu doués avec leur clavier en affichant un train en ascii qui traverse lentement le terminal.
https://www.linux.org/threads/useless-but-cool-command-line-tools-for-linux-deepin.51257/


### Sous interface graphique 

#### xeyes
xeyes est tellement inutile qu'il est installé par défaut avec la plupart des distributions : il s'agit d'un programme qui affiche des yeux qui suivent la souris.


#### Les animaux virtuels et autres éléments virtuels de companies

 

Neko (ou Oneko, Neko 95 - Neko provient du Japonais ねこ qui signifie chat - ) est un companon de bureau sous forme de félin blanc . Créé dans les années 80 pour le modèle d'ordinateur japonais NEC PC-9801, il a été adapté en 1989 sur Macintosh par Kenji Gotoh, qui a également conçu ses animations de sommeil. Neko a ensuite été porté sur Windows 95 et d'autres plateformes comme Windows 3.1, Acorn, et divers systèmes Unix (ex : Next Step, GNU/Linux) par Masayuki Koba. Le code source de la version X a servi à David Harvey pour l’adaptation sur Windows 95.

Neko poursuit le curseur de la souris et, lorsqu’il l'attrape, commence à se toiletter avant de s'endormir près de celui-ci. Il reste endormi tant que le curseur ne bouge pas, reprenant son action dès que la souris se déplace.


https://fr.wikipedia.org/wiki/Neko_(logiciel)



https://www.cyberciti.biz/open-source/oneko-app-creates-cute-cat-chasing-around-your-mouse/
nostalgia tech windows toys (  The Abandoned Art Of Windows Desktop Toys  )  : https://www.youtube.com/watch?v=YgHjw2M9w8E

lien avec neko 



nostalgia tech windows jokes   ( The Forgotten Art Of Windows Joke Applications  ) :  https://www.youtube.com/watch?v=FkCxR3Iw4ng:


## Loisir avec du logiciel libre

### soundtracker

### demoscene libre






URL:     https://linuxfr.org/news/liberer-les-consoles-delivrer-les-jeux
Title:   Libérer les consoles, délivrer les jeux. Episode 1 : un coup d'oeil dans le rétro.
Authors: Dave Newton 🍺
         Chilin
Date:    2024-02-06T00:00:00+01:00
License: CC By-SA
Tags:    jeu_vidéo et retrogaming
Score:   1

# Introduction

Dans le monde dynamique du jeu vidéo, où les avancées technologiques semblent repousser sans cesse les limites de l'innovation, il est fascinant de constater que de nombreux passionnés se tournent vers le passé pour redécouvrir les joyaux oubliés du rétrogaming. Ce mouvement, souvent aussi désigné sous le terme de "néo-rétrogaming", incarne un retour aux sources, une célébration des classiques qui ont façonné l'industrie du jeu vidéo. Mais au-delà de la simple nostalgie, le néo-rétrogaming offre une opportunité d'explorer à la fois l’histoire du logiciel libre sous le prisme vidéoludique et les possibilités actuelles offertes par  l'esthétique des jeux rétro. Dans cette exploration, nous plongeons dans un univers où la curiosité créative, l’esprit d’analyse, la ténacité et la passion se conjuguent pour donner naissance à de nouvelles expériences de jeu, où le passé et le présent se rencontrent pour renouveler les expériences vidéoludiques. 

Nous examinerons les liens entre l'évolution du logiciel libre et l'industrie du jeu vidéo, puis nous nous pencherons sur le phénomène du néo-retrogaming. Enfin, nous proposerons une classification pour démêler les éléments caractéristiques du rétro et du néo.

# Histoire du jeux vidéos et logiciel libre  

## Spacewar! sur PDP-1

![Spacewar](https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Spacewar%21-PDP-1-20070512.jpg/640px-Spacewar%21-PDP-1-20070512.jpg?uselang=fr)

Au commencement de l'ère informatique, une ère de découverte et d'expérimentation a donné naissance à deux phénomènes captivants : les jeux vidéo et le mouvement du logiciel libre. Durant les années 1950 et 1960, alors que les ordinateurs faisaient leurs premiers pas dans le monde militaire et académique, une poignée de hackers passionnés osaient explorer le côté ludique de ces machines.

C'est ainsi que naquit le célèbre "Spacewar!" en 1962, sous la plume de Steve Russell, au MIT. Dans ce jeu, deux vaisseaux spatiaux se livrent à une bataille épique dans l'espace en deux dimensions. Créé à l'origine pour le DEC PDP-1, "Spacewar!" a bénéficié de l'ingéniosité de plusieurs membres du Tech Model Railroad Club du MIT. Il s'est rapidement répandu sur les ordinateurs de l'époque, devenant ainsi le premier jeu multijoueur de l'histoire à franchir les frontières des machines individuelles.

Dans "Spacewar!", les joueurs prennent les commandes de vaisseaux spatiaux et s'efforcent de réduire en miettes celui de leur adversaire dans un espace en deux dimensions. En plus des manœuvres tactiques, les joueurs peuvent utiliser des fonctionnalités telles que l'hyperespace pour échapper à leurs ennemis, mais avec un risque à la clé.

L'impact de "Spacewar!" sur l'industrie du jeu vidéo a été immense. Il a servi d'inspiration à de nombreux autres jeux et a été adapté sur une multitude de systèmes informatiques de l'époque. Son importance historique a été formellement reconnue en le classant parmi les dix jeux vidéo les plus significatifs de l'histoire, intégré à la liste Game canon de la bibliothèque du Congrès en 2007.

Le code source de "Spacewar!" a été publié dans le domaine public, ouvrant ainsi la voie à sa réutilisation. Cette disponibilité a conduit à sa réimplémentation sur diverses plateformes, que ce soit par émulation ou par recréation. Par exemple, une version en Java a été mise à disposition sur [SourceForge](https://sourceforge.net/projects/pdp1spacewar/). Cette réadaptation du jeu original sur ordinateur, fonctionnant sur un émulateur DEC PDP-1, est programmée en C++ et utilise wxWidgets pour son interface. Elle permet d'exécuter le code objet du PDP-1 utilisé dans le jeu original de 1962, s'appuyant sur la version Java précédemment disponible depuis le MIT.

Un package Arch Linux proposait également SpaceWar!, mais n'est plus disponible. Les raisons de cette indisponibilité ne sont pas explicitement mentionnées. Il reste à déterminer si d'autres distributions GNU/Linux offrent également cette option.

Hackaday propose une initiative de restauration de SpaceWar! en utilisant la version disponible sur GitHub de MattisLind : https://github.com/MattisLind/SPACEWAR. Cette restauration dépasse le simple aspect logiciel et comprend également des efforts de restauration matérielle, comme décrit dans cet article : https://hackaday.com/2015/04/08/restoring-a-vintage-pdp-1104-computer/.


Naturellement, plusieurs clones de SpaceWar! ont été développés sous licence libre, et certains ont été modernisés. Par exemple, vous pouvez consulter celui-ci : https://github.com/thiagoharry/spacewar (GPL3) ou celui la https://github.com/gaziduc/space-war?tab=readme-ov-file (Apache 2.0)

KDE a aussi sa propre version sous licence GPL-2.0+ nommée [kscapeduel](https://apps.kde.org/fr/kspaceduel/).

![kspaceduel](https://cdn.kde.org/screenshots/kspaceduel/kspaceduel.png)


## lien avec star control 
lien avec star control et urquan mster , lien avec roman urquan master

## lien avec pong atari 

https://en.wikipedia.org/wiki/Pong

## lien avec space invaders : 

https://fr.wikipedia.org/wiki/Space_Invaders

## Le lien avec asteroid 

Linus torvalds : game source with submarine and fish to avoid (link to shark with laserhead )

https://fr.wikipedia.org/wiki/Asteroids 


## quid lien avec les autres jeux vidéos d'arcade ? 



## Le lien avec star control 1 et 2 et urquan master 

lien avec star control et urquan master + novellization d'urquan master en libre 


le roman : 

https://tommisalminenbooks.com/groombridge-log/?fbclid=IwAR1SJIx28eMzgRcOn4b0EFjStqO1mbhkHE3bRNMd9aUzygsYMVLpjv47zs0 

https://tommisalminenbooks.com/eternal-doctrine/?fbclid=IwAR2sWtynTK4JYi0iEagCsBl1VMMUW8-w2-RpGuPSJIOTi6N-nR2-bE4j88Q 


### quid avec la notion de borne d'arcade elle meme (mettre une pièce et c'est partie )

Computer Space is a space combat arcade video game released in 1971. Created by Nolan Bushnell and Ted Dabney in partnership as Syzygy Engineering, it was the first arcade video game as well as the first commercially available video game. Computer Space is a derivative of the 1962 computer game Spacewar!, which is possibly the first video game to spread to multiple computer installations

https://en.wikipedia.org/wiki/Computer_Space 


synzyngi became atari 


#### link with demoscene : 

https://videogamehistorian.wordpress.com/tag/mouse-in-a-maze/ 


##### link with tx-0 , emulator , demoscene ? 

https://www.wizforest.com/tech/tx-0/

https://forums.atariage.com/topic/170205-early-game-emulation/ , spacewar first real video games, other demos of video games , birth of demo and demoscene 

###### spacewar port pc - home of underdogs 

https://homeoftheunderdogs.net/game.php?id=3916

also other link about history of spacewar inspiration : https://www.wheels.org/spacewar/index.html 

###### pdp emulator
https://retrocomputingforum.com/t/ken-thompsons-space-travel-running-on-pdp-7-emulator/1503/2


## De Space Travel  au développement d’Unix 



https://news.ycombinator.com/item?id=34664433


Le jeu Spacewar!, inspiré des romans de science-fiction de Lensman d'E. E. "Doc" Smith, a été la source d'inspiration de Ken Thompson pour la création d’Unix. Thompson a commencé à concevoir Unix après avoir travaillé sur un PDP-7 récupéré pendant son temps libre en 1968-69, motivé par son expérience de développement de Spacewar! et d'autres projets de programmation. Space Travel, un jeu d'exploration spatiale basé sur du texte développé chez Bell Labs, a également joué un rôle crucial. Thompson a porté ce jeu sur plusieurs systèmes informatiques, ce qui l'a amené à développer son propre système d'exploitation pour faciliter la tâche. Ce système d'exploitation, qui a évolué à partir du projet de portage de Space Travel, est devenu Unix, l'un des systèmes d'exploitation les plus influents de l'histoire de l'informatique. Ainsi, le lien entre le développement historique d’Unix et le jeu vidéo Spacewar! illustre l'importance des interactions entre le domaine du jeu vidéo et celui de l'informatique dans le développement de technologies fondamentales.

Space Travel, un jeu développé sur un PDP-7 en 1969 par Ken Thompson et Dennis Ritchie, a été crucial pour le début de l'histoire de Unix. Leur travail a conduit à la création du système d'exploitation Unix, qui continue d'influencer l'industrie informatique jusqu'à nos jours notamment avec le projet GNU, le noyau Linux et les miliers de variantes des distributions GNU/Linux, les projets BSD etc... 
Ainsi, que les projets de réimplémentation de système d'exploitation basé sur le modèle Unix (les unix-like dans le retrocomputing )
tels que AROS , Haiku etc .... 

 Le Computer History Museum célèbre le 50e anniversaire de Unix en rendant publics certains des premiers codes sources Unix, dont ceux associés à Space Travel. 



Le jeu Spacewar!, inspiré des romans de science-fiction de Lensman d'E. E. "Doc" Smith, a été une source d'inspiration majeure pour Ken Thompson dans la création d’Unix. Thompson a commencé à concevoir Unix après avoir travaillé sur un PDP-7 récupéré pendant son temps libre en 1968-69, motivé par son expérience de développement de Spacewar! et d'autres projets de programmation. En parallèle, Space Travel, un jeu développé sur un PDP-7 en 1969 par Thompson et Dennis Ritchie, a également joué un rôle crucial dans le début de l'histoire de Unix. Leur travail sur ces jeux a conduit à la création du système d'exploitation Unix, qui est devenu l'un des systèmes les plus influents de l'histoire de l'informatique. Pour célébrer le 50e anniversaire de Unix, le Computer History Museum rend publics certains des premiers codes sources Unix, y compris ceux associés à Space Travel. Ce lien entre le développement historique d’Unix et le jeu vidéo Spacewar! met en lumière l'importance des interactions entre le domaine du jeu vidéo et celui de l'informatique dans le développement de technologies fondamentales. 

https://archive.computerhistory.org/resources/access/text/2019/09/102785108-05-001-acc.pdf 

https://computerhistory.org/blog/the-earliest-unix-code-an-anniversary-source-code-release/


# Développement de l’industrie du jeux vidéos

Pendant les années 1970 et 1980, l'industrie du jeu vidéo a commencé à prendre de l'ampleur avec l'avènement des consoles de jeux domestiques telles que l'Atari 2600 et le Commodore 64, ainsi que l'essor des bornes d'arcade. Les jeux vidéo étaient principalement distribués sur des supports physiques et les entreprises gardaient le contrôle sur leur code source.

Pendant ce temps, dans les années 1980, le mouvement du logiciel libre commençait à émerger. Richard Stallman, a fondé le projet GNU en 1984, avec pour objectif de créer un système d'exploitation entièrement libre GNU. Stallman a également développé la licence GNU GPL (General Public License), qui garantit aux utilisateurs la liberté d'utiliser, de modifier et de redistribuer les logiciels.

Dans les années 1990, l'avènement d'Internet a permis une diffusion plus rapide des idées et des logiciels libres. Le noyau Linux, développé par Linus Torvalds en 1991, est devenu un élément central des systèmes d'exploitation basés sur Unix. Les jeux vidéo n'étaient pas exclus de cette tendance, et certains développeurs ont commencé à expérimenter avec des licences de logiciels libres pour leurs jeux.
 
# Approche “Just for Fun” de Torvalds du logiciel libre et lien avec les jeux vidéos
 


## Just for fun Utile, participatif et amusant

A completer Venant de la biographie de Torvalds , intervieuw avec Diamond

Les jeux ayant influencé la création du noyau Linux (asteroid , pacman, Prince of Persia , voir autre si retrouve magazine ou Torvalds a publié le code source de ses jeux )

Selon Linus Torvalds :
Les vrais hackers sacrifient leur sommeil, leur santé physique, le match de foot du petit dernier, et de temps à autre le sexe aussi parce qu’ils adorent programmer. Ils aiment savoir qu’ils participent à un projet global (Linux est le projet collaboratif le plus vaste du monde), dont le but est de concevoir la meilleure technologie possible pour la mettre ensuite à disposition de tous. C’est aussi simple que cela et c’est amusant.


“Ce n’est pas parce que cent personnes utilisent mon code que je me sentais important, c’était juste que c’était amusant. Et c’est comme cela que cela doit etre” p 128

(...) ce n' est pas tant pour savoir qui m ‘écrivait que pour m’assurer que pour m’assurer que le problème avait été réglée ou quelle question passionnante j’allais devoir résoudre ce jour la

p.129





Trois éléments selon Torvalds  participe à sa vision du sens de la vie, (la survie  (le coté utile de régler les problèmes qui ont menté à créer le noyau Linux et que Minix ne permettait pas de régler ) , le social (Linux en tant que participation à un projet mondiale d’amélioration technologique bénéfique)  et l’amusement (Just for Fun)  .


Ainsi, les développeurs de logiciels libres et les membres de la scène néo-rétrogaming partagent en effet plusieurs qualités clés qui les aident à réussir dans leurs activités respectives. Voici comment la curiosité, la ténacité et l'esprit d'analyse sont des traits communs à ces deux mouvements :
 
Nous observons ainsi des caractéristique commune aux des développeurs de logiciel libre et à la  la scène de néo-retrogaming :

1. Curiosité: Les deux groupes sont souvent motivés par une curiosité insatiable pour comprendre le fonctionnement des systèmes et des jeux. Les développeurs de logiciels libres sont constamment à la recherche de nouvelles solutions techniques et d'approches innovantes pour résoudre les problèmes, tandis que les membres de la scène néo-rétrogaming sont curieux de découvrir et de redécouvrir les mécaniques de jeu et les technologies utilisées dans les jeux vidéo classiques.

2. Ténacité : Face aux défis et aux obstacles, les développeurs de logiciels libres et les joueurs-programmeurs de la scène néo-rétrogaming font preuve de ténacité. Ils sont prêts à consacrer du temps et des efforts considérables pour surmonter les difficultés techniques et pour atteindre leurs objectifs, que ce soit en développant un nouveau logiciel ou en réparant un vieux jeu vidéo.

3. Esprit d'analyse: Les deux groupes ont développé un solide esprit d'analyse qui leur permet de résoudre les problèmes de manière méthodique et efficace. Ils sont capables de décomposer les problèmes complexes en étapes plus petites et de les aborder pas à pas, en utilisant des techniques d'analyse et de débogage pour identifier et corriger les erreurs.

En combinant ces qualités, les développeurs de logiciels libres et les passionnés de la scène néo-rétrogaming sont en mesure de relever les défis techniques et créatifs auxquels ils sont confrontés, et de contribuer de manière significative à leurs communautés respectives.



# Stallman et les jeux vidéos

Richard Stallman, initiateur du mouvement du logiciel libre, prévoyait d'inclure un jeu, Empire, dès le [manifeste GNU en 1983](https://www.gnu.org/gnu/manifesto.html) bien qu'il ne fut libéré que [13 ans plus tard](https://lwn.net/Articles/946775/) sous le nom de [Wolfpack Empire](https://www.empire.cx/).

Depuis le projet GNU a intégré une petite vingtaine de [jeux vidéos](https://www.gnu.org/manual/manual.html#Games) dont le très amusant [Liquidwar](https://www.gnu.org/software/liquidwar6/).

![LiquidWar screenshot](https://www.gnu.org/software/liquidwar6/images/liquidwar6-screenshot-pizzaface-20111707.jpg)

Concernant l'arrivée de jeux privateurs sous GNU/Linux, Richard [a exprimé ses positions en priorisant le code libre sur l'art libre](https://www.gnu.org/philosophy/nonfree-games.en.html).

Si cette distinction code/art est plutôt curieuse et ne va pas sans poser de [problèmes](https://web.archive.org/web/20191125215630/http://onpon4.github.io/articles/gaming-trap.html), elle ouvre toutefois la porte aux [clones libres de jeux privateurs](https://osgameclones.com/).


----

# Les restrictions imposées aux joueurs

TODO: lister tout ce qui peut embêter les joueurs :-)

## DRM

La [gestion numérique des restrictions](https://fr.wikipedia.org/wiki/DRM) ou DRM prends diverses formes dans le monde du rétrogaming.

Généralement utilisé pour restreindre directement les droits des joueurs, l'une des plus célèbres dans l'histoire est le [CIC](https://en.wikipedia.org/wiki/CIC_(Nintendo)) de Nintendo qui vise plutôt les développeurs : cette puce a pour de les forcer à obtenir l'approbation de "Big N" pour faire tourner un jeu sur la [NES](https://en.wikipedia.org/wiki/Nintendo_Entertainment_System). L'argument à l'époque était d'empêcher la sortie de jeu de piêtre qualité (à entendre plutôt au sens "buggé" que "mauvais", comme le montre les sorties de Paper Boy, Dragon's Lair ou Zelda II). Pourtant le système n'est pas sans conséquence pour les joueurs : les éditeurs reporte la "taxe" Nintendo sur les joueurs, certains renoncent à sortir leurs jeux, les homebrews sont empêchés ainsi que le droit à la copie de sauvegarde.

Ce dernier droit est souvent celui visé par les DRM sous couvert de lutte contre le piratage. Jusqu'ici les DRM ont toujours fini par être crackés comme celui de la PS1 avec des [modchips](https://blog.kchung.co/making-playstation-modchips/) ou sans comme [sur Saturn](https://gbatemp.net/threads/the-sega-saturn-drm-has-been-cracked-after-twenty-years.434065/).

Certaines consoles n'ont pas de DRM, comme la [PC Engine de NEC](https://fr.wikipedia.org/wiki/PC-Engine), car à l'époque reproduire une [hucard](https://fr.wikipedia.org/wiki/HuCard) ou graver un CDROM demande un matériel si cher que NEC a du estimer que ce serait inacessible à l'immense majorité des joueurs.

## Le zonage

## Obsolescence matérielle, logicielle

## Exclusivité

## Lien avec des services en ligne

Récemment Nintendo a annoncé la mise à mort de ses services en ligne pour les consoles [3DS et Wii U](https://en-americas-support.nintendo.com/app/answers/detail/a_id/63227/~/announcement-of-discontinuation-of-online-services-for-nintendo-3ds-and-wii-u). Heureusement une alternative libre a été [lancé](https://pretendo.network/).

# Les problèmes pour le bien commun

## Difficultés de préservation du patrimoine

## Accès inégal selon les pays ?

## Sauvegardes : Konami veut le monopole du coeur

Si aujourd'hui les logiciels d'édition de sauvegarde ou le partage de sauvegarde est monnaie courante, certains éditeurs les ont combattu activement au nom du respect de leurs oeuvres d'art.

Dans une affaire concernant la *dating sim*, l'éditeur [Konami est allé en justice](http://gaming.moe/?p=2938) pour faire interdire la vente de cartes mémoires contenant des sauvegardes pour son jeu déclenchant l'ire des joueurs souhaitant profiter de certains romances du jeu sans y passer les milliers d'heures nécessaires.

...

# Et avec les quatres libertés ?

TODO: montrer à quels problèmes précédemment décrit avoir les libertés d'une licence libre serait une solution. Peut être avec un schéma ?

## Portages

### Amiga CD32 : les fans de Commodore se consolent

L'Amiga CD32 a reçu des portages de jeux Amiga : https://www.rgcd.co.uk/2011/04/unofficial-amiga-cd32-conversions-amiga.html

# Et en attendant ?

TODO: parler du compromis actuel => piratage, abandonware, gros hacks...

Leak de code source : https://tcrf.net/Category:Games_with_uncompiled_source_code

## Edition de sauvegarde



## Copie privée

La sauvegarde de jeux sur cartouches demande un périphérique spécial. Les premiers furent les [Game backup device](https://en.wikipedia.org/wiki/Game_backup_device) qui permettaient de "dumper" la ROM d'un jeu sur une disquette et de la restaurer dans une mémoire embarquée.

Aujourd'hui on utilise des cartes SD ou des transferts via USB. Certains dispositifs sont opensource comme le [cartreader](https://github.com/sanni/cartreader) à base d'Arduino.

A quelques protections prêt, la copie de jeux sur CD ou DVD est plus facile. Pour les consoles utilisant un disque spécial (comme le [GD-ROM](https://dcemulation.org/index.php?title=A_History_of_Media_and_Booting_on_the_Sega_Dreamcast) de 1 Gio sur Dreamcast par exemple), il faut souvent recourir à un hack de la console : pour restaurer la sauvegarde, on sera obligé de recourir à un CD-ROM de 650 Mio en compressant si besoin certains données (textures, son) pour que ça rentre ou bien de modifier la console pour installer un [lecteur de carte SD](https://gdemu.wordpress.com/about/).

# Conclusion

TODO: proposer de débattre, des ouvertures sur d'autres sujets?
