URL:     https://linuxfr.org/news/newton-adventure-1-2
Title:   Newton Adventure 1.2
Authors: devnewton
         Nÿco, baud123, Benoît Sibaud, Florent Zara et NeoX
Date:    2012-02-10T00:05:04+01:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure et jeu
Score:   24


Une nouvelle version de Newton Adventure, le jeu de plateforme 2D libre pour Linux, Windows ou Mac OS X qui vous permet de changer la gravité et faire tourner les niveaux à 360° est disponible. Pour connaître les principes de Newton Adventure, nous vous conseillons de (re)lire les précédentes dépêches (voir les liens). Pour les nouveautés, c'est la suite de la dépêche qui vous éclairera !

----

[Site officiel](https://play.devnewton.fr)
[LinuxFr.org : Un nouveau jeu libre : Newton Adventure](https://linuxfr.org/news/un-nouveau-jeu-libre-newton-adventure)
[LinuxFr.org : Sortie de Newton Adventure 1.1](https://linuxfr.org/news/sortie-de-newton-adventure-11)
[LinuxFr.org : Sortie du jeu libre Newton Adventure 1.0](https://linuxfr.org/news/sortie-du-jeu-libre-newton-adventure-10)

----

![Screenshots](http://tof.canardpc.com/view/9d20c369-2a93-45b3-a5fd-967eaa7f6886.jpg)


# Les nouveautés #


## Musiques ##


Grâce à http://opengameart.org j'ai pu ajouter des musiques pour tous les niveaux et écrans du jeu.


## Scores ##


À la fin de chaque quête, un score est attribué au joueur. Ce score peut être envoyé au serveur de score, soit en anonyme, soit avec un nom de joueur à configurer dans les options du jeu. Le score est déterminé par le nombre de pommes reçues ou perdues, les niveaux traversés, les ennemis tués et les pièces collectées.


## Niveaux bonus ##


Pour ajouter une possibilité pour faire un _highscore_, j'ai ajouté un ensemble de niveaux bonus. Ceux-ci sont accessibles en collectant toutes les pommes d'un niveau et en trouvant un téléporteur. Dans les niveaux bonus, il n'est plus possible d'utiliser le changement de gravité, il faut donc utiliser des plateformes mouvantes ou rebondissantes pour collecter un maximum de pièces en moins d'une minute.


## Menu d'options ##


Jusqu'ici Newton Adventure n'était configurable qu'en éditant un fichier et en relançant le jeu. Un menu d'options est maintenant accessible depuis l'écran d'accueil.


## Portage sur Android en cours ##


J'ai commencé un portage sur Android, il fonctionne peut être, mais ne possédant pas un téléphone de ce type, je n'ai pu le tester qu'avec l'émulateur fourni par Google. Malheureusement ce dernier est si lent qu'il est impossible de faire un développement OpenGL sérieux avec… N'hésitez pas à faire un don pour que je puisse m'en offrir un :-)

# Dégâts collatéraux #


Le développement de cette nouvelle version m'a amené à créer deux nouveaux logiciels libres.


## libtiled-android ##


libtiled-android est un portage de libtiled-java, une bibliothèque destinée à lire les fichiers de l'éditeur de niveau [tiled](https://www.mapeditor.org/) pour l'API Android.

## scoreserver ##


scoreserver est une application Django qui permet de gérer des highscores pour un ou plusieurs jeux.


# Futurs développements #


Portage sur téléphone et améliorations graphiques, le futur de Newton Adventure se heurte à un problème financier. Outre le mobile Android à acquérir, j'ai demandé un devis à un graphiste et il estime qu'il faut entre 3&nbsp;500 et 4&nbsp;500€ pour refaire l'ensemble des sprites et décors du jeu… 


À votre bon cœur!


![haha le pauvre](https://nsfw.totoz.eu/img/poor)



