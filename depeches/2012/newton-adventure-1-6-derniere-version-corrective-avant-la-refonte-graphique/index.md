URL:     https://linuxfr.org/news/newton-adventure-1-6-derniere-version-corrective-avant-la-refonte-graphique
Title:   Newton Adventure 1.6: dernière version corrective avant la refonte graphique
Authors: devnewton
         Xavier Teyssier, Benoît et tuiu pol
Date:    2012-06-21T10:26:06+02:00
License: CC By-SA
Tags:    newton_adventure
Score:   30


Newton Adventure est un jeu libre de plateforme 2d mettant en scène un personnage (Newton) ayant la faculté de modifier la gravité. Le joueur peut ainsi faire tourner les décors et atteindre des plateformes qui sont hors de portée dans un jeu classique.
Cette version corrige quelques bugs gênants et sera probablement la dernière avant la refonte graphique.

Voici les principaux changements:


 * correction d'un bug qui rendait le menu principal très très lent sur cette configuration ;
 * ajout d'un panneau dans chaque premier niveau de chaque quête permet d'afficher l'aide pour les 99% de joueurs qui ne lisent pas l'aide avant de jouer ;
 * correction d'un bug qui amenait Newton à être coincé dans un mur par un tir de canon ;
 * affichage du score récolté avec diverses actions dans le jeu ;
 * correction de la détection de collision avec les pièges mobiles.

----

[Site officiel (lent)](https://play.devnewton.fr)

----

Le jeu et ses sources (code sous licence BSD, données sous licence CC-BY-SA) sont téléchargeables sur le site du projet ou sur le plus rapide site de secours.

# Le futur #


La prochaine version arrivera sûrement dans un bon bout de temps, refondre tous les graphismes va être un gros travail! Ci dessous un extrait des premiers essais faits par Feust:

![preview_refonte](http://tof.canardpc.com/view/dc737d96-72cb-42fd-bd1e-3cfacec2beb3.jpg)


Cette refonte ne sera pas que visuelle: le moteur graphique va évoluer pour passer d'un système de tuiles à un système plus libre ( http://javilop.com/gamedev/c-game-programming-tutorial-non-tile-based-arbitrary-positioned-entity-engine-editor-like-in-braid-or-aquaria-games/ )

# Le portage android #


Après mes premiers essais sur le téléphone HTC Magic, j'ai mis en pause le portage android, car je ne pense pas pouvoir optimiser suffisamment Newton Adventure pour cette petite machine. Je le reprendrais si je mets la main sur un périphérique plus puissant ou si je trouve de l'aide pour optimiser le code.
