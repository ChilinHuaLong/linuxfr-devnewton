URL:     https://linuxfr.org/news/sortie-de-newton-adventure-11
Title:   Sortie de Newton Adventure 1.1
Authors: devnewton
         baud123, Nÿco et NeoX
Date:    2011-12-17T21:45:46+01:00
License: CC By-SA
Tags:    jeux_linux, java, lwjgl, phys2d, pape, newton_adventure et jeu
Score:   31


Newton Adventure est un jeu de plateforme 2D où le joueur dirige Newton, un héros très particulier, puisqu'il a le pouvoir de modifier la direction de la gravité. Ainsi pour Newton les niveaux du jeu ne sont pas linéaires et peuvent être parcourus dans tous les sens. Les ennemis et les objets subissent aussi les effets de la gravité, ce qui permet de les atteindre ou de les éviter en les faisant tomber dans la bonne direction.

----

[Site](https://play.devnewton.fr)

----

![Screenshot](montage_640_480.png)


Prix
====

Malheureusement pour moi, Newton Adventure est gratuit. Ceux qui voudraient quand même contribuer à son développement peuvent faire un don (en bière).


Les nouveautés
==============


Plus de contenu
---------------


Newton Adventure propose désormais 21 niveaux regroupés par quête :


- Artic : un monde enneigé où les plateformes sont très glissantes.
- Egypt : un univers désertique qui était déjà présent dans la version précédente.
- Jungle : des niveaux plus verts relativement faciles pour bien débuter et comprendre les mécanismes du jeu.
- Vatican : on ne trouvera pas le Pape au milieu de ces ruines antiques, mais un ensemble de pièges et de mécanismes qui donnent du fil à retordre au joueur.


Dans tous ces niveaux, on croisera de nouveaux types de plateformes : rebondissantes, glissantes ou traversables.


Plus configurable
-----------------


En éditant le fichier data/config.properties, on peut maintenant régler les touches de contrôle et certains paramètres d'affichage.


La technique
============


Newton Adventure est toujours écrit en Java avec les bibliothèques lwjgl pour l'affichage et phys2d pour la physique. Les niveaux sont éditables avec tiled et un greffon adapté. Les sources de Newton Adventure sont sous licence BSD tandis que les graphismes et les données du niveau relèvent de la Creative Commons BY-SA 3.0 ou de simple autorisation d'utilisation de leur auteur.


Le futur
========


Les prochaines évolutions que je prévois sont :


- créer un boss pour chaque quête.
- trouver un financement pour payer un graphiste afin d'avoir une vraie identité visuelle.
- porter le jeu sur mobile.


Remerciements
=============


Un grand merci aux artistes d'Opengameart et à Nicosmos (qui a dessiné le menu principal) qui m'ont permis d'avoir des graphismes corrects pour ce jeu.
