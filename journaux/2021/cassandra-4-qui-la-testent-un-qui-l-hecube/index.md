URL:     https://linuxfr.org/users/devnewton/journaux/cassandra-4-qui-la-testent-un-qui-l-hecube
Title:   Cassandra 4 qui la testent, un qui l'Hécube
Authors: devnewton
Date:    2021-08-04T16:01:38+02:00
License: CC By-SA
Tags:    bigdata, nosql, apache, cassandra, diversité et sexisme
Score:   20


Bonjour Nal,

Je t'écris pour te signaler la sortie de la base de données [Apache Cassandra 4.0](https://cassandra.apache.org/_/blog/Apache-Cassandra-4.0-is-Here.html).

Cette base orientée colonne grecque est devenue l'un des stockages les plus utilisées pour les titanodonnées. Écrite en Java (pour les perfs), elle utilisable via son langage de requête CQL ou via des API dans tous les bons langages mais aussi en Python ou Node.js.

Voici les nouveautés de cette version:

- le support de Java 11 : c'est important, car il s'agit de la dernière version Long Term Support ;
- des tables virtuelles (par exemple pour exposer une configuration en CQL) ;
- des logs plus complets (audit et requêtes) ;
- beaucoup d'optimisations ;
- un truc qui s'appelle Transient Replication, je n'ai pas compris ce que s'est, mais mon DBA a dit "super, je suis trop heureux" en l'apprenant.

Et toi Nal, tu as quoi comme base en prod?
