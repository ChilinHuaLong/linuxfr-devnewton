URL:     https://linuxfr.org/users/devnewton/journaux/je-veux-pas-y-retourner
Title:   Je veux pas y retourner
Authors: devnewton
Date:    2021-06-03T09:19:17+02:00
License: CC By-SA
Tags:    télétravail, capitalisme_de_surveillance et bureau
Score:   94


Ah Nal,

Les temps comme les œufs sont dur: la ministre du travail a indiqué la fin du télétravail [le 9 juin](https://www.journaldunet.com/management/guide-du-management/1200083-teletravail-combien-de-jours-sur-site-a-partir-du-9-juin/).

Le patronariat prépare avec délectation le retour en apotravail le plus vite possible: avoir ses collaborateurs sous la main permets de justifier le salaire de bien des managers inutiles.

Ça me rends triste, car je bosse beaucoup mieux chez moi: pas d'openspace bruyant, une connexion internet qui marche au poil sans proxy nazi, du bon café en grains... Je me vois aussi mal perdre à nouveau 2h par jour dans les transports ou devoir à nouveau repasser des chemises et prendre une douche tous les jours.

On me souffle qu'il a des bons côtés comme voir ses collègues, mais je me débrouille pour retrouver les plus proches de chez moi autour d'un repas en terrasse entre midi et deux. De toute façon quand je vais au bureau c'est pour faire des audioconférences au casque...

Bref c'est la grosse déprime, Nal. Et toi, tu préfères le télétravail ou l'apotravail?
