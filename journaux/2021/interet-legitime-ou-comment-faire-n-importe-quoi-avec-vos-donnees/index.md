URL:     https://linuxfr.org/users/devnewton/journaux/interet-legitime-ou-comment-faire-n-importe-quoi-avec-vos-donnees
Title:   Intérêt légitime ou comment faire n'importe quoi avec vos données
Authors: devnewton
Date:    2021-12-17T09:34:25+01:00
License: CC By-SA
Tags:    rgpd et cnil
Score:   59


Ah Nal,

Depuis le début de la grande partie mondiale de chat covid, j'avais réussi à ne jamais avoir besoin d'être testé.

Mais il y a peu, une personne de mon entourage a perdu et j'ai du découvrir la fabuleuse exploration nasale.

J'ai pu aussi constaté qu'une fois de plus, la gestion des données personnelles, sensibles et de santé avec les lois de type RGPD, les certifications HDS et les RSSI vigilants... hé bien c'est du vent.

En résumé:

- je suis allé dans un labo privé, je n'ai rien signé, je n'ai donné mon consentement à rien du tout ;
- mes données se retrouvent dans le SI du labo (j'aurais préféré une pseudonymisation, mais c'est encore logique) ;
- le lien vers les résultats me sont transmis par SMS (canal non sécurisé) avec pour seule protection ma date de naissance ;
- toutes mes données sont transmises à SIDEP, un machin géré par l'Assistance publique-hôpitaux de Paris: WTF je n'ai jamais demandé ça, je ne vis même pas à Paris...
- au nom de l'intérêt légitime, SIDEP garde tout, pas d'opposition possible, on est RGPD perché, pouce et retouche pas son père ;
- SIDEP déverse automatiquement toutes mes données **pseudonymisées** (et pas anonymisées) dans le Health Data Hub hébergé par Microsoft : pour s'y opposer il faut envoyer par mail ou courrier (canaux non sécurisés): mon identité, NIR, date de naissance, adresse, sexe, nom de mon chien, parfum de glace préféré...

Tout cela est sans doute légal, mais on est très loin d'un monde privacy first où le labo devrait juste par exemple:

- conserver des données pour me contacter ;
- me fournir une url+token au moment où je passe le test pour venir les chercher plus tard ;
- que cette url renvoie juste une page avec oui/non ;
- ne pas transmettre mes données.

