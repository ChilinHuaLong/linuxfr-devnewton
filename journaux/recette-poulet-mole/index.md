URL:     https://linuxfr.org/users/devnewton/journaux/recette-poulet-mole
Title:   [Recette] Poulet Mole
Authors: devnewton
Date:    2014-07-19T22:44:10+02:00
License: CC By-SA
Tags:    cuisine, mexique, cuisinefrorg et recette_de_cuisine
Score:   5


Bonjour Nal,

J'ai terminé l'excellent mais priwateur [Guacamelee](http://guacamelee.com/), un jeu de plateforme baston mettant en scène un luchador au prise avec des monstres du folklore mexicain. Pour fêter ça, je me suis préparé du poulet mole dont voici la recette.

![Guacamelee](http://guacamelee.com/wp-content/uploads/2011/10/Launch_03.jpg)

1. Faire chauffer de l'huile.
2. Faire revenir de l'ail et de l'oignon hachés finement.
3. Ajouter des morceaux de poulets et laisser cuire jusqu'à ce qu'ils deviennent blancs.
4. Couvrir d'eau et ajouter tout ce qui traîne dans votre placard pour donner du goût: bouillons, épices, piments...
5. Porter à ébullition.
6. Retirer le poulet.
7. Baisser le feu et ajouter du chocolat noir râpé en évitant de laisser bouillir.
8. Goûter et ajuster la quantité d'épices et de chocolat jusqu'à obtenir un goût équilibré. Un peu de fécule de pomme de terre peut aider à épaissir la sauce.
9. Servir sur une tortilla de maïs avec du riz mexicain.

![Don't mess with the wrong mexican](http://tof.canardpc.com/view/cfe8118f-0fde-4032-93a0-dd6b65f1deda.jpg)

Bon appétit Nal!

![Vamos a la playa senor zorro](http://www.mexconnect.com/photos/10492-pollo-en-cuachala-s-original.jpg?1330290743)



