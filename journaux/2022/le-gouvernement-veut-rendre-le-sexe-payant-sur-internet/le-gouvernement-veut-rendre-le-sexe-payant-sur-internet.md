URL:     https://linuxfr.org/users/devnewton/journaux/le-gouvernement-veut-rendre-le-sexe-payant-sur-internet
Title:   Le gouvernement veut rendre le sexe payant sur internet
Authors: devnewton
Date:    2022-10-26T09:50:15+02:00
License: CC By-SA
Tags:    capitalisme_de_surveillance, sexe, erotisme_responsable, vie_privée, macbook et branlette
Score:   20


Ah Nal,

Alors qu'on double peine à faire comprendre à nos amis ukrainiens et russes qu'ils feraient mieux de faire l'amour et pas la guerre, notre gouvernement a de brillantes idées pour promouvoir l'érotisme responsable: le rendre payant.

Pratiquant [l'oral à l'assemblée](https://videos.assemblee-nationale.fr/video.12356956_635779217ef02.delegation-aux-droits-des-enfants--mme-charlotte-caubel-secretaire-d-etat-aupres-de-la-premiere-m-25-octobre-2022?timecode=856930), la secrétaire d'état Charlotte Caubel a expliqué vouloir rendre obligatoire d'une carte bancaire pour accès aux sites coquins.

(Attention ne montrez pas cette vidéo à vos enfants, on y voit un macbook tout nu).

Le but est de protéger les ~~enfants~~moins de 18 ans en sacrifiant la [vie privée des adultes
]( https://www.francetvinfo.fr/societe/pornographie/interdiction-des-sites-porno-aux-mineurs-quimporte-le-moyen-cest-lobjectif-qui-compte-declare-la-secretaire-detat-chargee-de-lenfance_5439451.html) en comptant sur des dispositifs légaux et techniques au détriment de l'éducation.

[Proche d'Edouard Philippe](https://www.gouvernement.fr/ministre/charlotte-caubel), elle suit sans doute une logique très à droite: ce qui est cher est de qualité. Le sexe tarifé est donc meilleur que le gratuit.
