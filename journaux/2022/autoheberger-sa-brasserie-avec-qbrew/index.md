URL:     https://linuxfr.org/users/devnewton/journaux/autoheberger-sa-brasserie-avec-qbrew
Title:   Autohéberger sa brasserie avec qbrew
Authors: devnewton
Date:    2022-07-02T16:12:44+02:00
License: CC By-SA
Tags:    brasserie, auto-hébergement, recette_de_cuisine et qbrew
Score:   23


Bonjour Nal,

Je t'écris pour te parler de [qbrew](https://tracker.debian.org/pkg/qbrew), un logiciel pour composer des recettes de bière.

![screenshot](qbrew.png)

Il permets de calculer à partir des ingrédients des caractéristiques importantes pour le rendu (amertume, degré d'alcool, couleur), mais aussi pour le brassage (densités initiales et finales).

Ma dernière recette exportée en markdown :

```
DarkDave85
----------
Brewer: Dave Newton
Style: Dry Stout
Batch: 8.00 L All Grain
License: WTFPL

Characteristics
---------------
Recipe Gravity: 1.087 OG
Recipe Bitterness: 39 IBU
Recipe Color: 39° SRM
Estimated FG: 1.022
Alcohol by Volume: 8.5%
Alcohol by Weight: 6.6%

Ingredients
-----------
American chocolate malt       0.40 kg, Grain, Mashed
Flaked oats                   0.10 kg, Adjunct, Mashed
German vienna                 2.00 kg, Grain, Mashed
Maris Otter Malt              0.60 kg, Grain, Mashed

Magnum                        11.00 g, Pellet, 60 minutes

Ale yeast                     1.00 unit, Yeast
```

Pour le brassage lui même, il suffit de suivre les étapes:

1. Concasser le malt ;
2. Faire chauffer à 67°C pendant une heure ;
3. Égoutter puis rincer les drêches ;
4. Faire bouillir pendant une heure avec le houblon ;
5. Laisser fermenter dans une dame Jeanne pendant un mois ;
6. Ajouter 7g de sucre par litre et mettre en bouteille ;
7. Attendre trois semaines et déguster !

![Le brassage en images](brassage.png)

Et toi Nal, qu'est-ce que tu as autohébergé ces temps-ci ?
