URL:     https://linuxfr.org/users/devnewton/journaux/spring-troie-est-dehors-le-cadriciel-java-n-a-plus-son-talon-d-achille
Title:   Spring Troie est dehors : le cadriciel java n'a plus son talon d'Achille
Authors: devnewton
Date:    2022-11-28T09:21:42+01:00
License: CC By-SA
Tags:    java, spring, graalvm et sortie_version
Score:   38


Bonjour Nal,

Je t'écris pour te parler de la nouvelle version du plus populaire des cadriciels Java : Spring.

Largement utilisé en entreprise, il est le grand vainqueur de la guerre des Trois milles frameworks, conflit des années 2000 durant lequel il avait écrasé des concurrents redoutables comme Struts, Play ou Wicket dans un combat homérique dont les cicatrices couvrent de nombreux projets legacy.

Mais depuis quelques temps, un nouveau venu faisait le Pâris de détrôner : Quarkus. Plus jeune et plus mince, il fait valoir sa réussite dans la quête du [GraalVM](https://fr.wikipedia.org/wiki/GraalVM) : compiler en natif pour ne plus dépendre de la lourde [JVM](https://fr.wikipedia.org/wiki/Machine_virtuelle_Java).

Spring voulait en faire autant, Ménélas ayant pris du poids avec l'âge, il lui fallu un régime de quelques années pour parvenir enfin à cette release: [Spring 3.0](https://spring.io/blog/2022/11/24/spring-boot-3-0-goes-ga).

Compilation native, Java 17, des bibliothèques à jour et une rétrocompatibilité plutôt bonne ( Illiade nombreux changements toutefois, lisez bien le [changelog](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.0-Release-Notes) ) le vieux framework est prêt pour régner encore de nombreuses années.

Et toi Nal, tu aimes GraalVM, bien que ce n'est pas Java même non?
