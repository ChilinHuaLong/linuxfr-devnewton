URL:     https://linuxfr.org/users/devnewton/journaux/taab-une-tribune-sans-xml
Title:   taab, une tribune sans XML
Authors: devnewton
Date:    2017-04-27T23:07:29+02:00
License: CC By-SA
Tags:    tribune, bouchot, tsv et branlette
Score:   22


Bonjour Nal,

Je t'écris pour te faire part de la release de [taab](https://framagit.org/very-old-devnewton-projects/taab), un nouveau moteur de tribune basé sur [vue.js](https://vuejs.org/), [pegjs](https://pegjs.org), [blazecss](http://blazecss.com/) and [php](https://totoz.eu/img/phpsuper) (désolé).

Les fritures de ce bouchot:

- sans XML, car le backend est conforme à la nouvelle ~~norme~~ coutume de la moulosphère: le [TSV](https://linuxfr.org/suivi/backend-tsv-pour-la-tribune).
- des norloges ISO 8601 ( YYYY-MM-DDTHH:mm:ss )
- gestion du paramètre lastId et de l'entête X-Post-Id.

Pour ceux qui se demandent ce qu'est une tribune, un bouchot, un backend XML & co, il s'agit du vocabulaire de la moulosphère, un ensemble de systèmes de messageries semi instantanées orientées vers l'incubation d'excellence, l'innovation disruptive et l'insoumission de la France.

La plus fréquentée est celle de [linuxfr](https://linuxfr.org/board), mais il en existe beaucoup d'autres: les meilleures accessibles avec mon autre projet moulesque, [gb3](https://gb3.devnewton.fr).

++ Nal
