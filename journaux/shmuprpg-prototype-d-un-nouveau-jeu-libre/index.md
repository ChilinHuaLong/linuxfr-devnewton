URL:     https://linuxfr.org/users/devnewton/journaux/shmuprpg-prototype-d-un-nouveau-jeu-libre
Title:   Shmuprpg: prototype d'un nouveau jeu libre
Authors: devnewton
Date:    2016-11-04T23:24:15+01:00
License: CC By-SA
Tags:    playn, typescript, brunchio, phaserio, panpan, jeu et jeu_libre
Score:   37


Bonjour Nal,

Après [Newton Adventure](https://play.devnewton.fr) et [Ned et les maki](http://geekygoblin.org/ned-et-les-maki/), je me lance dans le développement d'un nouveau jeu libre.

Ayant plusieurs projets en tête, j'ai décidé de faire des prototypes pour tester mes idées de gameplay, voici le premier: [shmuprpg](https://play.devnewton.fr/prototypes.html)

Il s'agit d'un [shoot them up](https://fr.wikipedia.org/wiki/Shoot_%27em_up) qui emprunte la forme graphique d'un [action rpg](https://fr.wikipedia.org/wiki/Action-RPG). On y joue soit avec le combo clavier/souris, soit avec les sticks analogiques d'une manette, ce qui en fait un Dual Stick Shooter.

Sous licences libres ( MIT pour le code et CC-BY-SA pour les médias, le jeu est une application web écrite avec [Typescript](https://www.typescriptlang.org/), [brunch](https://brunch.io) and Phaser (https://phaser.io). Bien que l'ensemble soit moins puissant/pratique que Java et les technos que j'ai utilisé pour mes autres jeux, le packaging et la distribution d'applications non web étant un [cauchemar](https://linuxfr.org/users/devnewton/journaux/write-once-run-anywhere-qu-il-disait), j'ai préféré privilégier une solution plus proche d'html5 pour rendre mes prototypes accessibles en un simple clic.

Pour mes anciens jeux, j'ai utilisé [playn](https://playn.io/) pour en faire des versions web (mais la compilation est assez lente) et mis le tout accessible sur un seul site: [play.devnewton.fr](https://play.devnewton.fr).

Ah Nal, c'est triste, mais c'est comme ça: web is the new console.

PS: le chargement des jeux est assez lent, soyez patient.
