URL:     https://linuxfr.org/users/devnewton/journaux/integrer-scoreserver-en-ajax
Title:   Intégrer scoreserver en ajax
Authors: devnewton
Date:    2013-05-18T12:38:13+02:00
License: CC By-SA
Tags:    newton_adventure, scoreserver et jeux_linux
Score:   6


Bonjour Nal,

Je t'écris pour te faire part d'une petite évolution de mon serveur de gestion de scores, le bien nommé scoreserver.

Il était déjà capable de:

- gérer la création de jeu, de niveau et l'édition des scores et des joueurs avec une interface d'administration.
- recevoir et mettre à jour les tables des meilleurs scores via une simple requête http.
- présenter sur le web ces meilleurs scores par jeu et par niveau ou par joueur:

![pasbelle](frontend.png)

Cette interface n'étant pas très belle, j'ai fait en sorte qu'il soit maintenant possible d'intégrer ces tables dans d'autres pages via une simple requête ajax (par exemple avec [jQuery](http://jquery.com/)):

    <h1>Scores</h1>
    <div id="newton_adventure_highscore">
    </div>
    
    <script>
    function load_newton_adventure_highscore() {
        jQuery('#newton_adventure_highscore').load('/scoreserver/ajax_game/newton_adventure/');
    }
    jQuery(document).ready(load_newton_adventure_highscore);
    </script>

Le serveur réponds ensuite avec les tables des scores:

    <table>
        <tr><th colspan="2">Arctic</th></tr>
        <tr>
            <td>398</td><td>anonymous</td>
        </tr>
        
        <tr>
            <td>100</td><td>devnewton</td>
        </tr>    
        <tr>
            <td>30</td><td>bloub</td>
        </tr>
        <tr>
            <td>10</td><td>devnewton2</td>
        </tr>
        <tr>
            <td>-32</td><td>Feust</td>
        </tr>
        <tr><th colspan="2">Egypt</th></tr>
        <tr>
            <td>1436</td><td>Feust</td>
        </tr>
        <tr>
            <td>669</td><td>anonymous</td>

Voici ce que ça donne sur le site de Newton Adventure:

![scores na](ajax.png)




