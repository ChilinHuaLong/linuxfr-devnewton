URL:     https://linuxfr.org/users/devnewton/journaux/opensara-plop-plop
Title:   Opensara? Plop ! Plop !
Authors: devnewton 🍺
Date:    2023-06-02T16:50:12+02:00
License: CC By-SA
Tags:    jeu_vidéo, jeu_libre, futur et tux
Score:   0


Bonjour Nal,

Je t'écris pour te présenter le nouveau niveau de mon jeu [opensara](https://play.devnewton.fr/opensara/).

Il se passe dans un futur dystopique où des robots tournant sous GNU/Linux Debian Stable, on prit le pouvoir. Un futur assez proche et assez probable si l'on en croit [certains](https://linuxfr.org/users/patrick_g/journaux/une-simulation-de-drone-de-combat-qui-tourne-mal).

Ce niveau complète les autres courtes aventures de Sara< qui sont à jouer pendant une pause café, un peu comme on lit ~~une bande dessinée en une page à la fin d'un journal papier aux toilettes~~ un webcomic depuis sa liseuse couleur avant d'utiliser les [trois coquillages](https://archive.org/details/thingiverse-3363707).

![train](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/2023/opensara-plop-plop/opensara.png)

Les sources du projet sont sur [gitlab](https://gitlab.com/davenewton/opensara) toujours sous des licences libres.

Amuse toi bien et à plus dans le musk, Nal.
