URL:     https://linuxfr.org/users/devnewton/journaux/cahier-de-doleances
Title:   Cahier de doléances
Authors: devnewton
Date:    2019-01-14T10:55:46+01:00
License: CC By-SA
Tags:    débat, doléance, démocratie, république, gilets_jaunes, réforme et politique
Score:   23


Bonjour Nal,

Tu sais que depuis quelques mois l'enthousiasme pour les réformes de notre gouvernement est visible en jaune sur de nombreux ronds points. Pour ne pas laisser les gens dehors dans le froid, notre cher président a décidé de lancer un [gros débat](https://www.gouvernement.fr/le-grand-debat-national) dans tout le pays. De plus pour que ces braves gens n'attrapent pas de rhume de cerveaux, il a aussi listé des questions qui contiennent les réponses dans une [lettre aux Français](https://www.lemonde.fr/politique/article/2019/01/13/document-la-lettre-d-emmanuel-macron-aux-francais_5408564_823448.html).

L'espace [numérique et disruptif](https://granddebat.fr/) de ce gros débat n'étant pas encore prêt, je te propose d'utiliser les commentaires de ce nourjal comme un grand cahier de doléances digital et collaboratif pour que les moules et autres utilisateurs de linuxfr puissent faire des propositions pour réformer notre beau pays.

Les autres ethnies (belge, suisse, québécois...) peuvent bien sûr participer ! L'internationalisme n'a jamais tué personne.
