URL:     https://linuxfr.org/users/devnewton/journaux/jb3-1-0
Title:   Jb3 1.0
Authors: devnewton
Date:    2017-03-12T17:05:04+01:00
License: CC By-SA
Tags:    
Score:   -1


Ah Nal,

Je voulais te parler longuement de la release 1.0 de Jb3, la tribune la plus friturée du marché, mais un sanglier vient de ravager pour la 3° fois mon jardin et je dois ~~mettre des mines et du barbelé~~ renforcer mon grillage.

Je me contenterais donc d'un journal [bookmark vers l'annonce en anglais](https://framagit.org/very-old-devnewton-projects/jb3).

A C++ dans le bus!

![Pan pan](hog_cburr.jpg)
