URL:     https://linuxfr.org/users/devnewton/journaux/gnome-l-outbreak-apres-l-outreach
Title:   Gnome, l'outbreak après l'outreach?
Authors: devnewton
Date:    2014-04-13T17:45:56+02:00
License: CC By-SA
Tags:    gnome, féminisme, argent, don et appeau_a_zenitroll
Score:   5


Bonjour Nal,

Je t'écris pour te signaler que ton environnement de bureau préféré est en danger.

En effet, la fondation Gnome n'a plus un rond, essentiellement, car le succès du _Outreach Program for Women_ a été plus important que prévu.

Plusieurs solutions sont envisagés dans l'esprit du projet:

- mettre un bouton _faire un don_ obligatoire au login.
- saborder le network manager des créanciers.
- supprimer les femmes et l'argent du système mondial.

https://wiki.gnome.org/FoundationBoard/CurrentBudgetFAQ
