URL:     https://linuxfr.org/users/devnewton/journaux/java-9-est-dehors
Title:   Java 9 est dehors
Authors: devnewton
Date:    2017-09-22T11:12:13+02:00
License: CC By-SA
Tags:    openjdk, java et oracle
Score:   43


Bonjour Nal,

Je t’écris pour t’informer de la sortie de la nouvelle et très attendue version majeure de [Java](https://docs.oracle.com/javase/9/), l’une des plus grosses plates‐formes de développement du marché. Voici un petit tour des nouveautés :
    
![Titre de l’image](java.png)

Victime de jmod
===============
La principale nouveauté est l’introduction d’un système de modules. Ce système mérite un journal complet, mais le principal apport sera le « _debloat_ » (un peu) de l’environnement d’exécution et des applications Java.

Dans les poèmes de jshell
=========================
Un outil [_jshell_](https://docs.oracle.com/javase/9/jshell/introduction-jshell.htm) permet de jouer avec le langage en ligne de commande et utiliser le langage pour faire du [script](https://docs.oracle.com/javase/9/jshell/scripts.htm) : pratique pour remplacer des scripts Shell ou Python !
    
```
    jshell> double PI = 3.1415926535
    PI ==> 3.1415926535
    |  created variable PI : double
    
    jshell> volume(2)
    |  attempted to call method volume(double) which cannot be invoked until method cube(double) is declared
    
    jshell> double cube(double x) { return x * x * x; }
    |  created method cube(double)
    |    update modified method volume(double)
    
    jshell> volume(2)
    $5 ==> 33.510321637333334
    |  created scratch variable $5 : double
```

Applet mon numéro
=================
Une excellente nouvelle : le greffon Java pour navigateur va rejoindre Flash et Silverlight dans le purgatoire des technologies pénibles.

What string color
=================
Les chaînes de caractères prendront [moins de place](http://openjdk.java.net/jeps/254) en mémoire si elles contiennent des caractères latin 1.

Ramasse‐miettes, chouette, c’est sympa
======================================
G1 sera désormais le ramasse‐miettes par défaut. L’idée est de privilégier la faible latence. Des impacts sont à prévoir sur les applications en production : il faudra faire des tests sérieux pour voir s’il y a un gain ou perte, chaque application ayant des besoins et des objectifs différents.

Trololog
========
Une nouvelle API de [journalisation minimaliste](http://openjdk.java.net/jeps/264) a été ajoutée. Je n’ai pas compris pourquoi ce changement, l’API actuelle n’étant pas bien compliquée.

UTF-8 vaincra
=============
Les fichiers `*.properties` pourront enfin être écrits en [UTF-8](http://openjdk.java.net/jeps/226). Ces fichiers, souvent utilisés pour l’internationalisation, étaient limités à l’ISO-8859-1 (latin 1)…

Autres changements
==================
- les chauves seront contents d’apprendre l’ajout d’une API pour manipuler des [TIFF](http://openjdk.java.net/jeps/262) ;
- la gestion des hautes résolutions (HiDPI) permettra de ne plus saigner des yeux sous [GNU/Linux et Windows](http://openjdk.java.net/jeps/263) ;
- GTK 3 est enfin géré, il faudra toutefois l’activer via la propriété `jdk.gtk.version`.
