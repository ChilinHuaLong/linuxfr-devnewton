8iǐURL:     
Title:   Les inutilitaires libres
Authors: devnewton, chilin
Date:    2024-02-12T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   1

# Ramener du fun dans la communauté du Libre



Bonjour Nal,

Aujourd’hui, nous (Devnewton et ChilinHuaLong) souhaitions explorer le concept des inutilitaires libres. Dans une époque où l'efficacité est souvent perçue comme ayant le monopole du discours, il nous semble important de s'arrêter un instant pour apprecier l'inutilité en tant qu approche ludique et  fun dans le Libre.


## Petit philo  de l inutile 

L'obsession de l'efficacité soulève plusieurs questions. D'abord, celle du résultat : un objectif peut être atteint de manière très efficace, mais cela ne garantit pas qu'il soit moralement acceptable. Les régimes totalitaires du XXe siècle en sont un triste exemple. Ensuite, il y a la présomption de l’inutilité de l'inutile. L’idée qu'il est toujours nécessaire de maximiser son rendement ne tient pas compte des moments où l'efficacité émerge de l’absurde ou de l'inattendu.  l'exemple typique est l Eureka dans la baignoire d'Archimède ou de la pomme de Newton : des moments de sérendipité, où l'inutile, ou du moins ce qui semblait tel, a mené à une avancée majeure. Enfin, la recherche incessante de l'efficacité peut parfois nous faire oublier que l'inutile a aussi sa place. Ce sont souvent ces moments "d'inefficacité" qui apportent de la joie et permettent de redécouvrir des choses plus profondes, loin de la productivité.

Dans ce contexte, il nous semble pertinent de nous pencher sur des logiciels libres qui n'ont pas de but utilitaire immédiat, mais qui, à leur manière, apportent un peu de légèreté et de créativité à notre quotidien. En effet, certains logiciels qui ne semblent pas avoir de fonction "productive" peuvent néanmoins nourrir notre curiosité et stimuler notre imaginaire. C'est là que l'inutile devient indispensable, et c'est ce que Nuccio Ordine nous invite à considérer dans son livre L’utilité de l'inutile. Pour lui, l'inutile a une valeur inestimable, car il nous permet d'échapper à la pression de l'efficacité et de trouver du sens dans des moments où l'on ne cherche pas forcément à être productif.

L’art, la curiosité, le cote anecdotique des situations serieuses : toutes ces pratiques n'ont pas d'autre but que celui d’exister pour elles-mêmes. C'est précisément cette absence d'objectif immédiat qui en fait des sources essentielles d'enrichissement personnel. Ordine souligne que l'humain, par sa capacité à être inefficace, trouve une forme de richesse qui dépasse la simple recherche de résultats tangibles. Enfin, ces réflexions ne sont pas nouvelles. La philosophie orientale, dès le IIIe siècle avant J.-C., soutenait déjà que l’inutile avait une valeur profonde. Comme le disait Zhuang Zi à propos d’un arbre jugé inutile, mais qui, par son inadaptation à l’usage humain, a échappé à la destruction et a trouvé sa place dans la nature, loin des exigences utilitaires. Cet arbre vivait en toute liberté, offrant de l'ombre et un refuge, sans jamais être "utile" au sens traditionnel.C'est ainsi que  Zhuang Zi constata que  « Les gens comprennent l’utilité de ce qui est utile, mais ignorent celle de l’inutile. »

Et vous, quel rapport entretenez-vous avec l'inutilité ? Vous autorisez-vous parfois à être inefficace, à faire des choses juste pour le plaisir, sans objectif précis ? N'hésitez pas à partager vos expériences. Quels moments d’inutilité ont ouvert la voie à des découvertes ou à de nouvelles idées ? Prenons ensemble un instant pour nous éloigner de l'efficacité et savourer l’inutile en visitant ou en revisitant les inutilitaires classiques dans l'open source, ainsi que d'autres petites découvertes sans autre prétention que d'apporter un peu de légèreté dans le quotidien.


source 

https://www.lesbelleslettres.com/livre/9782251444888/l-utilite-de-l-inutile#:~:text=Si%20on%20ne%20comprend%20pas,pas%20le%20rire%2C%20il%20y
https://le-carnet-et-les-instants.net/archives/leys-le-studio-de-l-inutilite/#:~:text=%C2%AB%20Les%20gens%20comprennent%20tous%20l,du%20iiie%20si%C3%A8cle%20avant%20J.

https://ando.life/journal/the-useless-tree 

 
 

## Les inutilitaires en ligne de commandes 


### fortune

La bonne fortune dans le terminal

Inspiré des fameux fortunes cookies des restaurants chinois, il génère des messages aléatoires, qu'il s'agisse de citations, proverbes ou blagues, apportant une touche d'humour et de réflexion à chaque exécution. De nombreux administrateurs système le configurent pour qu'une nouvelle fortune soit affichée à chaque ouverture de terminal. Ce logiciel, qui s'inscrit dans un clin d'œil aux traditions orientales, trouve un écho particulier lors des festivités du Nouvel An chinois (cette annee le 29 janvier 2025), période où les symboles de chance et de bonne fortune sont omniprésents. En ajoutant une touche d'humour geek, et parfois un peu de non-sens, fortune s’intègre parfaitement dans cette atmosphère festive , et peut être personnalisé avec la commande cowsay (cf section idoine), qui ajoute une dimension ludique en faisant parler une figure en ASCII. Ainsi, fortune devient bien plus qu'un simple inutilitaire, mais une petite tradition numérique qui égaye le quotidien des utilisateurs de Linux.

![fortunes.cookies](https://gracca.github.io/images/fortune-linux.png))

### pyjoke

pyjoke permets d'afficher des blagues. Malheureusement, il y en a peu [en français](https://github.com/pyjokes/pyjokes/blob/main/pyjokes/jokes_fr.py). Peut être que les moules pourraient y contribuer ?

    $ pyjoke 
    If you put a million monkeys at a million keyboards, one of them will eventually write a Java program. The rest of them will write Perl.

## Les décoratifs ASCII art

Au début de l'ère informatique, les écrans n'affichaient que des caractères ASCII, en fait les caractères du clavier : pas d'images ni de sons. C'était une époque où nous étions encore loin de l'âge du multimédia, déjà prédit par le professeur Marshall McLuhan. Le chat était déjà de rigueur, et les salons IRC et les BBS  fleurissaient. Ainsi vint le succès des émoticônes ;-) : ces caractères prirent leur envol grâce à la communication textuelle virtuelle.

Ensuite, vinrent l'art ASCII/ANSI, qui consiste en une succession de caractères formant une image intelligible. Les artistes numériques et autres hackers rivalisaient d'ingéniosité pour afficher sur l'écran ces œuvres cyber-expressionnistes. La demoscene a aussi récupéré cette nouvelle forme d'art.

Bien que l'on distingue rarement l'ASCII art en catégories comme on le ferait en peinture, deux "courants" divergent :

- Les "schématistes", dont les caractères forment des schémas, comme les émoticônes qui représentent une émotion.
- Les "réalistes", qui s'inspirent de médias antérieurs (photos, dessins animés, 3D, bandes dessinées, logotypes, films) et ne font que remplacer les pixels par des caractères ASCII.

Pour l'approche schématique, chaque caractère a son importance, autant que sa forme. Pour les réalistes, seule compte la forme globale : les caractères peuvent être substitués sans changement majeur (on peut, par exemple, remplacer tous les "v" par des "b" sans que l'image subisse une grande modification). En revanche, dans une émoticône – la forme la plus simplifiée – si l'on change une seule lettre, tout est transformé. Dès lors, nous allons revisiter ces deux approches dans les inutilitaires libres .

---

## Ascii art schématique 


### Moon-Buggy

Certains jeux ont ainsi été concue comme le célèbre jeux nethack , Moon Buggy lui aussi possède un graphique réduit a sa plus simple expression Vous incarnez un pilote de vehicule lunaire qui doit éviter les cratères et detruire les rochers , la jouabilité est bonne et celui ci garde le charme de son game play . Ce jeu est une version ASCII de Moon Patrol, un classique de l’arcade des années 80. Un retour rétro fun et léger.


---

### sl

cL'inutilitaire " Sl " , sort du lot. en effet  celui ci fait rappel amusant aux  dyslexiques ou aux utilisateurs peu doués avec leur clavier en affichant un train en ascii qui traverse lentement le terminal. En effet il n'est que la fameuses commande  " ls " (listing ) inversé qui  affiche une locomotive traversant le terminal a tout allure ,  avertissants l'utilisteur que sa  vitesse de frappe au clavier a engendré une inversion de lettrage.

![sl](sl.png)

---

### Cbonsai

Envie de cultiver un bonsaï, mais vous n'êtes pas un expert en jardinage ? Cbonsai est là pour vous ! Ce programme permet de faire pousser un bonsaï directement dans votre terminal. Un petit projet zen pour adoucir vos sessions de travail.

![cbonsai](cbonsai.png)

---

### Asciiquarium

Si vous aimez les poissons mais que votre appartement est déjà trop plein d'animaux (chat, chien, hamster…), Asciiquarium vous permet de créer un aquarium animé dans votre terminal. C’est comme avoir un petit coin de nature, sans les inconvénients du bruit des poissons.

![asciicquarium](asciicquarium.png)

---

## Hollywood Hacker Screen Style

Dans presque tous les films hollywoodiens, on voit des hackers tapant sur leur clavier, entourés de lignes de code, de diagrammes et de chiffres qui défilent rapidement sur un terminal. On va recréer cette scène culte dans la réalité grâce à un outil simple qui transforme votre terminal Linux en un véritable terminal de hacking à la Hollywood, en temps réel.


[Hollywood Hacker Screen Style - IT's FOSS](https://itsfoss.com/hollywood-hacker-screen/)

![Hollywood Hacker Screen Style](https://github.com/bcbcarl/docker-hollywood/raw/master/screenshot.png)

---


### Cmatrix

Dans la suite de l'esprit de la représentation du hacker par Hollywood un inutilitaire inspirée de Neo, ainsi si vous avez aimé le film Matrix ,  ou du moins, si vous aimez l'idée de voir des caractères verts tomber à toute vitesse sur votre écran vous amuse ?  Cmatrix vous permet de recréer cette animation. À vous de vous glisser dans la peau de Keanu Reeves, avec des lunettes noires et un air aussi expressif qu'un parpaing.

[cmatrix](https://github.com/abishekvashok/cmatrix)

![cmatrix](https://raw.githubusercontent.com/abishekvashok/cmatrix/master/data/img/capture_orig.gif)

---
### Lolcat

Lolcat est une commande qui fonctionne comme cat pour afficher du texte dans la ligne de commande, mais avec un petit twist : le texte s'affiche avec des couleurs arc-en-ciel, comme un véritable mème. Parfait pour ajouter un peu de gaieté dans un monde de terminal parfois un peu trop gris.

![lolcat](https://raw.githubusercontent.com/busyloop/lolcat/master/ass/screenshot.png)

---

## L'ascii art  schematique dans les README et les commentaires de codes sources

Que ce soit dans un terminal ou un fichier texte comme un README (ou dans les commentaires d'un code source), ces dessins en ASCII ne se contentent pas d’ajouter une touche esthétique : ils donnent du relief aux mots. En encadrant, soulignant et mettant en avant les points clés, ils attirent immédiatement l'œil et transforment un simple texte en une  mini mise en scène visuelle 

### Boxes 

boxes créer un contour soignieux , et varié .

mettant en évidence la partie concerné . le contour se compose soit d'un simple rectangle , soit d'une image autour .

```plaintext


 __   _,--="=--,_   __
         /  \."    .-.    "./  \
        /  ,/  _   : :   _  \/` \
        \  `| /o\  :_:  /o\ |\__/
         `-'| :="~` _ `~"=: |
            \`     (_)     `/
     .-"-.   \      |      /   .-"-.
.---{     }--|  /,.-'-.,\  |--{     }---.
 )  (_)_)_)  \_/`~-===-~`\_/  (_(_(_)  (
(        Different all twisty a         )
 )         of in maze are you,         (
(           passages little.            )
 )                                     (
'---------------------------------------'
```

### Cowsay

Cowsay est un outil en ligne de commande qui permet de faire parler une vache en ASCII art. Voici un exemple :

```plaintext
$ cowsay Bonjour Nal
 _____________
< Bonjour Nal >
 -------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

---

### Figlet

Si l'art ASCII vous plaît, alors figlet est l'outil qu'il vous faut. Ce programme transforme votre texte en énormes caractères ASCII stylés. Avec une variété de polices disponibles, vous pouvez créer des designs impressionnants en un clin d'œil, et c'est un excellent moyen de personnaliser vos messages ou de rendre vos scripts un peu plus fun.

Ansi Figlet permet d'afficher le texte que vous lui donnez en une police schématisé, celle ci permet la génération des titres rapidement et avec un minimum d'effort.

```plaintext
 _____ ___ ____ _      _   
|  ___|_ _/ ___| | ___| |_ 
| |_   | | |  _| |/ _ \ __|
|  _|  | | |_| | |  __/ |_ 
|_|   |___\____|_|\___|\__|
```

---


## calcul sa propre circonference 

## L'approche réaliste de l'ascii art 
  

### Demo BB 

L'ASCII art s'est largement répandu dans les cercles des demo makers, ces passionnés qui n'ont pas résisté à l'envie de repousser les limites artistico-techniques de ce médium. Ainsi, une demo sublime en son genre, mêlant comme le veut la tradition musique électronique, animation et programmation, offre un spectacle digne de ce nom. La demo nous entraîne dans un voyage qui semble tout droit sorti d’Alice au pays des merveilles psychedeliques.

L'anecdote  l'histoire de la Demo BB est lié à la volonté d'avoir un logo sur GNU/Linux : 

Tout a commencé quand deux amis, passionnés d’informatique, ont voulu afficher un logo Linux sur leurs vieux écrans Hercules.  
Petit souci : ces écrans ne supportaient pas la couleur. Pour contourner le problème, ils ont tenté de convertir l’image en ASCII art... mais le premier résultat était catastrophique.  

C’est alors que l’un d’eux a développé un nouvel algorithme pour améliorer la conversion.  
Plus tard, en travaillant sur *XaoS* (un zoomer fractal), l’idée d’appliquer l’ASCII art aux ensembles de Mandelbrot a émergé – et le rendu était bluffant !  

####  Objectifs du projet AA  

-  Adapter des logiciels iconiques comme *Doom* ou *X Windows* à l’ASCII art.  
-  Porter AA-lib sur un maximum de plateformes.  
-  Convaincre IBM de recommencer la production des cartes MDA.  

Pour démontrer la puissance de cette technologie, une démo nommée *BB* a été créée.  
Depuis, le projet est libre et ouvert à tous.  

Une belle aventure entre technique et créativité, où chaque ligne de code devient un coup de pinceau numérique !  

https://github.com/denisse-dev/bb 
---

### Libcaca 

Libcaca est un autre projet dans la perspective que AALib , mais avec support plus étendu  : 

- Support de l’Unicode
- Jusqu'à 2048 couleurs disponibles (contre seulement 16 sur certains dispositifs)
- Tramage des images en couleur
- Opérations avancées sur la grille de texte (blitting, rotations)

Le plus intéressant est la raison de la création de libcaca  donné par les développeurs , qui semble être une ôde à l'inutilitaire : 

```plaintext

Pourquoi ? 

    Que dites-vous ?... C’est inutile ?... Je le sais !
    Mais on ne se bat pas dans l’espoir du succès !
    Non ! non, c’est bien plus beau lorsque c’est inutile !
    -- Edmond Rostand, Cyrano de Bergerac 


Je suis parfaitement conscient que libcaca ressemble à une pure perte de temps. Inutile de me le rappeler. Je vous invite à lire la préface de Mademoiselle de Maupin de Théophile Gautier, qui offre également une excellente explication du nom libcaca.

    Il n’y a rien de vraiment beau que ce qui ne peut servir à rien ; tout ce qui est utile est laid ; car c’est l’expression de quelque besoin ; et ceux de l’homme sont ignobles et dégoûtants, comme sa pauvre et infirme nature. - L’endroit le plus utile d’une maison, ce sont les latrines. 

```

http://caca.zoy.org/wiki/libcaca

![libcaca](https://upload.wikimedia.org/wikipedia/en/9/9b/Libcaca-logo.png)

Source :  
[Command Line Fun](https://hamvocke.com/blog/commandline-fun/)  
[Forum Linux Mint](https://forum-francophone-linuxmint.fr/viewtopic.php?t=3051)
[Moon-Buggy](https://www.seehuhn.de/pages/moon-buggy.html)


## convertisseur en ascii art avec affichage des nombres premiers


https://github.com/benjaminorthner/asciiPrime

![asciiPrime](https://raw.githubusercontent.com/benjaminorthner/asciiPrime/refs/heads/main/readme_images/screenshot6.png)


## explorer les liens de hacker news  notamment l ascii art scene et la bbs scene 

https://news.ycombinator.com/item?id=31891226


Voir le format de fichier pour l ascii et ansii art sur BBS id.diz et ceux associer 

https://fr.m.wikipedia.org/wiki/FILE_ID.DIZ

## Les décoratifs graphiques

### xeyes

xeyes est tellement inutile qu'il est installé par défaut avec la plupart des distributions : il s'agit d'un programme qui affiche des yeux qui suivent la souris.

![xeyes](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Blackbox-Fenstermanager.png/800px-Blackbox-Fenstermanager.png)

### xjokes

Cette collection de logiciels inutiles pour X11 propose d'affichage un trou noir, des filles qui font un clin d'oeil et de faire disparaître l'écran.

![xjokes](https://community.linuxmint.com/img/screenshots/xjokes.png)

 
## Les animaux virtuels et autres éléments virtuels de companies

#### gkrellm-bfm

Ce programme de monitoring dispose d'un canard qui sert... de compagnon?

![gkrellm-bfm](gkrellm-bfm.png)

#### Neko (comparaison avec Necko sous Windows)

Neko (ou Oneko, Neko 95 - Neko provient du Japonais ねこ, signifiant "chat") est un compagnon de bureau sous forme de félin blanc. Créé dans les années 80 pour le modèle d'ordinateur japonais NEC PC-9801, il a été adapté en 1989 sur Macintosh par Kenji Gotoh. Neko a ensuite été porté sur Windows 95, ainsi que sur d'autres plateformes comme Windows 3.1, Acorn et divers systèmes Unix (comme Next Step ou GNU/Linux) par Masayuki Koba. Le code source de la version X a été utilisé par David Harvey pour l’adaptation de Oneko sur GNU/Linux vers Neko 95 (Windows 95).

Le principe de Neko est simple : il suit le curseur de la souris, et lorsqu'il l'attrape, il commence à se toiletter avant de s'endormir près de celui-ci. Il reste endormi tant que le curseur ne bouge pas, reprenant son action dès que la souris se déplace à nouveau.

![Neko](https://upload.wikimedia.org/wikipedia/commons/b/b1/Neko_animation_steps_-_falling_asleep.png?uselang=fr)

Le projet Neko illustre bien comment un logiciel libre peut être porté sur différentes plateformes. Cependant, en explorant les inutilitaires, on remarque qu'il n'existe pas toujours de version libre pour tous les programmes. Certains ont été portés sous GNU/Linux, mais d'autres n'ont pas eu cette chance. Cela amène à se poser la question suivante : quels inutilitaires propriétaires vous manquent en version libre ? Et qu'en est-il des anciens inutilitaires, utilisés sous des systèmes comme Windows 3.1 ou XP, qui, bien qu'oubliés, suscitent encore un peu de nostalgie ?

---

#### XPenguins

XPenguins, créé par Robin Hogan, permet d’ajouter des petits pingouins, inspirés des graphismes du jeu Pingus (https://pingus.seul.org/), qui marchent sur les bords de vos fenêtres. Vous pouvez même envoyer une armée de pingouins envahir l'écran d'un autre utilisateur sur votre réseau.

Site de XPenguins : [http://xpenguins.seul.org/](http://xpenguins.seul.org/)

![xpenguins](https://community.linuxmint.com/img/screenshots/xpenguins-applet.png)

---

#### Xteddy

Xteddy est un programme qui a joué un rôle crucial pour son créateur en l’aidant à traverser des périodes d'anxiété sociale et de troubles paniques. Ce logiciel offre un compagnon virtuel sous forme d'un petit animal de bureau. Séduit par ce programme, son créateur a contacté l'auteur original et trouvé une version physique sur eBay. Cela l’a motivé à créer le site xteddy.org. Bien que le site de l'auteur original ne soit plus actif, des versions comme Teddy sous Linux ont vu le jour, ouvrant la voie à de futurs développements.

Site de Xteddy : [http://weber.itn.liu.se/~stegu/xteddy/xteddy_info.html](http://weber.itn.liu.se/~stegu/xteddy/xteddy_info.html)

![xteddy](xteddy.png)

---

#### Xroach

Xroach est un jeu classique qui consiste à afficher des cafards répugnants sur votre fenêtre principale. Ces petites créatures se déplacent frénétiquement jusqu'à trouver une fenêtre sous laquelle se cacher. À chaque fois que vous déplacez ou réduisez une fenêtre, les cafards se précipitent à nouveau pour se mettre à l'abri.

Site de Xroach : [https://github.com/interkosmos/xroach](https://github.com/interkosmos/xroach)

---

#### Xsnow

XSnow est un autre programme bien connu qui ajoute de la neige virtuelle tombant sur votre bureau, créant ainsi une ambiance hivernale à l'écran. C’est un classique qui ajoute une touche de fraîcheur à vos sessions de travail.

Site de XSnow : [https://sourceforge.net/projects/xsnow/](https://sourceforge.net/projects/xsnow/)

![xsnow](https://upload.wikimedia.org/wikipedia/commons/4/4b/Xsnow.png)

---

#### LOVE Amor

Amor est une interface permettant de gérer des animaux virtuels sous KDE. Cette application permet d'interagir avec différents animaux de bureau, ajoutant un peu de vie et d’interactivité à l’environnement de travail.

Image de Amor :  
![Amor Config](https://blog.desdelinux.net/wp-content/uploads/2014/08/amor-config.png)

---

#### Doggo

Doggo est une IA de chien, développée en Python avec Pygame. Le chien se déplace aléatoirement sur l’écran, change de direction et d’état, et sa couleur de pelage varie de manière aléatoire. Les changements d’état suivent une chaîne de Markov. Ce projet est né de l’envie d’un collègue de l’auteur, qui ne pouvait pas avoir de chien, de créer un compagnon virtuel tout en explorant les chaînes de Markov.

Site de Doggo : [https://github.com/u8slvn/doggo?tab=readme-ov-file](https://github.com/u8slvn/doggo?tab=readme-ov-file)



## assistant virtuel 
Les assistants virtuels sont des personnages qui sont l'équivalent humain ou anthropomorphe des animaux virtuels et qui ont soit une fonction esthétique, soit parfois une fonction anecdotique.

### Macopix 

MaCoPiX (Mascot Constructive Pilot for X) est l'application de mascottes ultime pour les systèmes UNIX et X Window. Vous pouvez y créer des petites créatures qui se posent sur vos fenêtres, des mascottes fixes qui s'installent confortablement sur votre bureau, et même des mascottes horloges pour vous tenir compagnie tout en affichant l'heure. De quoi rendre votre bureau un peu plus vivant et amusant !
 
 
https://rosegray.sakura.ne.jp/macopix/index-e.html

https://directory.fsf.org/wiki/Macopix

![macopix](https://rosegray.sakura.ne.jp/macopix/pic/ss_conf.jpg)

### Virtual RMS  

vrms est un programme en ligne de commande pour Debian et ses dérivés, comme Ubuntu. Il permet de vérifier la proportion de paquets libres et non libres installés sur le système. Écrit en Perl, il détecte les paquets "non-free", c'est-à-dire ceux qui imposent des restrictions sur l'utilisation ou la modification. Le programme liste ces paquets et explique pourquoi ils ne respectent pas les critères de liberté définis par Debian. Si aucun paquet non-libre n'est trouvé, un message indique que « rms serait fier ». Le nom "vrms" fait référence à Richard Stallman, mais suit la définition de la liberté logicielle de Debian, qui peut différer de celle de Stallman.

  
vrms  https://fr.m.wikipedia.org/wiki/Vrms#:~:text=vrms%2C%20Virtual%20Richard%20M.,est%20publi%C3%A9%20sous%20licence%20GPL.

 
## tamagochi-like :  Open hardware tamgochi 


Dans la séries animaux de companies virutel , dans les années 90 il y a eu une vague à la mode de tamagochi , aujourd'hui il est possible de le faire avec du open hardware , nous avons répertorié deux projets en ce sens : TamaFi et Tamanooki : 


https://hackaday.io/project/201820-tamafi-reviving-the-classic-virtual-pet-with-a-te

https://github.com/cifertech/TamaFi 
 

https://www.instructables.com/Tamanooki-an-Arduino-Based-Virtual-Pet-tamagotchi-/

![tamafi](https://cifertech.net/wp-content/uploads/2024/12/MVI_7218.00_01_26_03.Still008-1024x576.jpg)



 ## Les écrans de veille : xscreensaver 




Dans les temps anciens, lorsque les écrans affichaient une image fixe trop longtemps, ils pouvaient être endommagés par un phénomène appelé combustion interne du phosphore. Pour prévenir ce problème, des utilitaires appelés "économiseurs d'écran" ont été créés. Bien que cette fonction soit aujourd'hui obsolète, les écrans de veille sont devenus des artefacts artistiques en soi, souvent appréciés pour leur côté créatif. Aujourd'hui, tous les [bons](https://docs.xfce.org/apps/screensaver/start) environnements de bureau en proposent, tandis que les [mauvais](https://mail.gnome.org/archives/gnome-shell-list/2011-March/msg00340.html) ont tendance à les retirer de leur offre.

Ainsi, malgré leur origine pratique, les économiseurs d’écran ont évolué et continuent d’offrir une valeur ajoutée au-delà de leur utilité initiale. C’est précisément dans ce contexte que XScreenSaver s'inscrit comme un exemple parfait de réinvention. En 2022, il a célébré son trentième anniversaire, prouvant qu'un bon logiciel peut traverser le temps et les générations. Lancé en 1992, il est rapidement devenu un incontournable pour les systèmes Linux et Unix utilisant le système de fenêtres X11. En plus de proposer une vaste collection d’économiseurs d’écran, XScreenSaver devient une sorte de musee de l informatique montrant les screensaver les plus vieux (comme la boule rouge d amiga ) jusqu au effet demo recente offrant une retrospective de ce qui se faisait au different age de l informatique. En somme, XScreenSaver est bien plus qu’une simple collection d’écrans de veille : c’est une courte contemplation retrospective esthétique de l evolution de l informatique  comme  peinture animée. 


https://www.jwz.org/xscreensaver/screenshots/
 
![xscreensaver](https://cdn.jwz.org/xscreensaver/screenshots/atlantis.jpg)

 
## compteur de kilometre pour la souris  Kodometer  

Kodometer est une application KDE qui vous permet de mesurer la distance parcourue par le curseur de votre souris sur le bureau. Ce programme, à l’interface qui ressemble à un véritable compte-kilomètres de voiture, propose un odomètre. Il vous permet de suivre votre distance totale ainsi que les trajets spécifiques que vous effectuez. Vous avez le choix entre des unités métriques ou américaines, selon vos préférences. Kodometer était à l'origine basé sur un ancien programme VMS/Motif appelé Xodometer, développé par Mark H. Granoff. Son portage vers KDE/C++ a été réalisé par Armen Nakashian, qui l’a découvert en voyant une version fonctionnant sur le bureau d’un collègue.

Malheureusement, Kodometer semble avoir été abandonné. Il n'est plus disponible dans les paquets KDE et les codes sources ne sont plus accessibles. Cela rend l’application difficile à obtenir et à faire fonctionner aujourd’hui. Bien que ce programme soit principalement ludique, il peut également être utile pour sensibiliser aux risques de problèmes musculo-squelettiques liés à un usage intensif du bureau, en vous aidant à prendre conscience de vos habitudes de mouvement.  

En résumé, Kodometer est une petite application sympathique , mais qui a été laissée dans l'oubli.
 


## Créer une table d'équivalence entre inutilitaires propriétaires et libres : une idée sympa !


Loin d'être exhaustif, ce rapide tour d'horizon met en lumière une réalité importante : bien que des programmes comme Neko montrent tout ce que le libre peut offrir, il existe encore des lacunes à combler pour certains inutilitaires.

En effet, certains inutilitaires propriétaires sont désormais obsolètes, abandonnés par leurs développeurs, et ne fonctionnent même plus sur les systèmes modernes. Pourtant, bien qu'ils ne soient plus mis à jour, ils n'ont pas encore été remplacés par des versions libres. Prenons l'exemple de Kodometer, un utilitaire KDE qui mesurait la distance parcourue par le curseur de la souris. Sympathique, mais malheureusement abandonné et sans code source. Un paradoxe pour un projet libre, non ? Ce genre de situation serait plus attendu dans le monde des logiciels propriétaires.

Cela dit, répertorier ces inutilitaires et identifier leurs alternatives libres est une démarche permettant de revisiter nos classiques , retrouver l esprit decouverte et anecdotes et finalement liberer son esprit le temps d un caffe de l efficacite a tous prix . D'autre part cela aide à voir ce qui manque dans l'écosystème du libre, et d'autre part, cela peut motiver ou remotiver à créer les solutions open-source pour combler ces vides. Une manière ludique et constructive de faire avancer le libre tout en s'amusant un peu !

Une idée à creuser : créer une table d'équivalence entre inutilitaires propriétaires et leurs alternatives libres. L'objectif ? Lister des inutilitaires bien connus et trouver leurs équivalents libres (ou souligner ceux qui n'existent pas encore) sur DLFP.

## Cher lectorat de DLFP, quel logiciel propriétaire vous manque toujours un équivalent libre ? Ou avez-vous déjà eu affaire à un logiciel libre abandonné, dont les codes sources sont introuvables, ou carrément galère à installer ?

Et une petite question bonus : quel est votre rapport à l'inutile ? En référence à l'intro, où l’on a évoqué les logiciels dits "inutiles", pensez-vous qu'il y a de la valeur dans ces outils, même s'ils semblent un peu obsolètes ou superflus ?



https://linuxfr.org/wiki/linuxfr-org
 