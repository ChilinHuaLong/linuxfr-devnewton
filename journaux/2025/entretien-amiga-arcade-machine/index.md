URL:     
Title:   Entretien avec Drush à propos de RGBDS
Authors: devnewton, chilin
Date:    2024-02-12T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   1


Le club belges  des utilisateurs  d'amiga a créer une borne d'arcade amiga , via la réutilisation d'une marchine Apple,
qui a été formaté avec une distribution linux pour le retrogaming : Batocera.linux  et ils ont créer les éléments de hardware qui vont avec pour avoir une look and feel d'une borne d'arcade  :
avant ils utilisaient un autre projet libre qui est la pandora box  (ce serait intéressant pour ceux qui sont dans la communauté pandora box de connaitre le type de fun et le niveau de fun chez eux ).

![Amiga](https://wiki.batocera.org/_media/joystick-black-title-subtitle-outlined.png?w=300&tok=894619)
![Amiga](https://www.amigaclub.be/api/image/_img/projects/arcade/bond.jpg)

----

[L'Amiga Arcade Machine](https://www.amigaclub.be/projects/arcade)
----

## Partie 1: présentation

### Qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?

### Pourquoi le retrogaming est-il important pour vous ?

## Partie 2: L'Amiga Arcade Machine

### Comment en êtes-vous venu à créer ce projet ?

### TODO Ajouter des questions spécifiques à l'Amiga et à sa "mise en arcade"

### Quels sont vos jeux commerciaux préférés sur Amiga ?

### Quels sont vos jeux "homebrew" préférés sur Amiga ?

## Partie 3: pour finir

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?

### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?

### Quelle question auriez-vous adoré qu’on vous pose ?

### Quelle question auriez-vous détesté qu’on vous pose ?
