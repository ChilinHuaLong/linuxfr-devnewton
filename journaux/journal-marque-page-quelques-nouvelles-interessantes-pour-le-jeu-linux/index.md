URL:     https://linuxfr.org/users/devnewton/journaux/journal-marque-page-quelques-nouvelles-interessantes-pour-le-jeu-linux
Title:   [journal marque page] quelques nouvelles intéressantes pour le jeu linux
Authors: devnewton
Date:    2014-03-13T10:31:45+01:00
License: CC By-SA
Tags:    jeux_linux et jeu
Score:   33


Bonjour Nal,

Ces derniers jours ont vu des annonces intéressantes pour les joueurs et développeurs de jeu sous Linux:

- Valve a libéré une couche pour porter les jeux direct3d sur opengl: https://github.com/ValveSoftware/ToGL
- Valve encore a libéré un debugger opengl: https://github.com/ValveSoftware/vogl
- Crytek annonce le portage de son fameux moteur de jeu, Cryengine, sur linux: http://www.bit-tech.net/news/gaming/2014/03/12/cryengine-linux/1

Petit à petit, l'industrie du jeu s'invite sur notre OS préféré. 

Au delà du domaine ludique, on assiste peut être à une grande convergence: bientôt on utilisera le même système pour jouer et travailler, du serveur au mobile en passant par la console de salon.
