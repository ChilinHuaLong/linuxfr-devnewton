URL:     https://linuxfr.org/users/devnewton/journaux/java-11-est-dehors
Title:   Java 11 est dehors
Authors: devnewton
Date:    2018-09-26T11:09:41+02:00
License: CC By-SA
Tags:    java
Score:   33


Bonjour Nal,

Je t'écris pour te parler de la sortie de la nouvelle version de la plateforme de développement préférée des développeurs de logiciels libres: [Java 11](http://openjdk.java.net/projects/jdk/11/).

Voici les nouveautés qui ont retenu mon attention:

- Les applets et la plateforme de déploiement Web Start, dépréciées en Java 9, sont définitivement mis à la poubelle. Java et les navigateurs web c'est fini!
- Windows et MacOS ne bénéficieront plus de mises à jour automatique.
- Le JRE disparait. Pour lancer une application Java, il faudra désormais soit utiliser le JDK, soit construire un mini runtime via [jlink](http://openjdk.java.net/jeps/282).
- [JavaFX](https://openjfx.io) et Java Mission Control deviennent des projets autonomes.
- une nouvelle API pour [faire des requêtes HTTP](http://openjdk.java.net/jeps/321) fait son apparition.
- il est désormais possible d'exécuter un simple fichier .java comme un script shell avec un [shebang](http://openjdk.java.net/jeps/330#Shebang_files).
