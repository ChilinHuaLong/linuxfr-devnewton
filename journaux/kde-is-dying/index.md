URL:     https://linuxfr.org/users/devnewton/journaux/kde-is-dying
Title:   KDE is dying
Authors: devnewton
Date:    2018-11-03T15:01:08+01:00
License: CC By-SA
Tags:    ibm, kde, dying, debian et red_hat
Score:   7


Ah Nal,

Je t'écris pour te faire part d'une triste nouvelle: Red Hat abandonne KDE!

> KDE has been deprecated
KDE Plasma Workspaces (KDE), which has been provided as an alternative to the default GNOME desktop environment has been deprecated. A future major release of Red Hat Enterprise Linux will no longer support using KDE instead of the default GNOME desktop environment.

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/7.6_release_notes/#chap-Red_Hat_Enterprise_Linux-7.6_Release_Notes-Deprecated_Functionality

La nouvelle tombe quelques jours après qu'IBM se soit acheté ce beau chapeau rouge, un hasard? Je ne crois pas !
