URL:     https://linuxfr.org/users/devnewton/journaux/recette-mantou
Title:   [Recette] Mantou
Authors: devnewton
Date:    2013-07-05T12:45:24+02:00
License: CC By-SA
Tags:    chinois, bondour et recette_de_cuisine
Score:   7


Bonjour Nal,

Hier soir j'ai cuisiné des [mantous](http://fr.wikipedia.org/wiki/Mantou), des petits pains chinois. En voici la recette.

Prérequis
=========

- un cuit vapeur.
- du papier sulfurisé.
- une friteuse (en option).

Ingrédients
===========

- 250g de farine
- 80ml d'eau
- un sachet de levure boulangère.
- un demi sachet de levure chimique

Procédure
=========

1. Mélanger la farine, l'eau et la levure boulangère. Pétrir jusqu'à obtenir une pâte homogène.
2. Couvrir et laisser reposer pendant 1h.
3. Pétrir à nouveau et former un rouleau de 25cm.
4. Découper en bouts de 5cm et les poser sur de petits morceaux de papier sulfurisé.
5. Cuire à la vapeur pendant 20min.
6. Faire frire (en option).

Dégustation
===========

Non frit, les mantous se consomment légèrement chaud et s'accompagnent bien de viandes et légumes, mais aussi de confiture ou de beurre.

[Mantou](http://upload.wikimedia.org/wikipedia/commons/0/08/ClassicwhiteMantou.jpg)


