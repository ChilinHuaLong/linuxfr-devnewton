URL:     https://linuxfr.org/users/devnewton/journaux/le-broutage-sur
Title:   Le broutage "sûr"
Authors: devnewton
Date:    2018-09-15T13:01:39+02:00
License: CC By-SA
Tags:    google et firefox
Score:   3


Bonjour Nal,

Je t'écris, car l'un mes sites est désormais considérer comme malveillant par un service d'une obscure [entreprise](https://fr.wikipedia.org/wiki/Alphabet_%28entreprise%29).

La raison de ce classement dans une blacklist est assez ironique:

> Le site comporte du contenu dangereux, notamment des pages qui :
> Incitent les internautes à fournir des informations personnelles ou à télécharger des logiciels

Hors le [cœur de métier](https://www.google.fr/about/products/
) de cette entreprise consiste à inciter les internautes à fournir des informations personnelles ou à télécharger des logiciels...

Je n'ai pas bien compris comment sortir de cette liste. En attendant je ne peux que te conseiller de désactiver le service "Safe Browsing" de ton brouteur préféré:

https://support.mozilla.org/en-US/questions/922449



