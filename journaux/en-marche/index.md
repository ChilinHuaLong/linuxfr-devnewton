URL:     https://linuxfr.org/users/devnewton/journaux/en-marche
Title:   En marche
Authors: devnewton
Date:    2017-09-23T07:54:38+02:00
License: CC By-SA
Tags:    chaviste
Score:   -12


Ah Nal,

J'ai oublié dans mon précédent journal de te rappeler qu'aujourd'hui est prévu une _marche contre le coup d'État social d'Emmanuel Macron, qui veut détruire le code du travail à coup d'ordonnances_.

![ordonnances](demainjaibastille-visuel-1-1024x512.jpg)

Outre les habituels défilés et revendications, la rumeur indique que Philippe Martinez et Pierre Laurent danseront le mambo sur l'air de _Je suis Sancho de Cuba_ pour clore la journée.

On prétends aussi que, jaloux, Jean Luc Mélenchon aurait fait venir l'orchestre national de Caracas par le premier avion (en seconde classe) afin d'entonner l'hymne vénézuélienne au même moment.

Quoiqu'il en soit, l'évènement sera expertisé en direct depuis la [tribune](https://linuxfr.org/board/).
