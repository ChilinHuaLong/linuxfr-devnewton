URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-1-15
Title:   Newton Adventure 1.15
Authors: devnewton
Date:    2014-05-18T00:31:07+02:00
License: CC By-SA
Tags:    newton_adventure, ned_et_les_maki et firefox
Score:   29


Bonjour Nal,

Je t'écris pour te signaler la sortie de la version 1.15 de  [Newton Adventure](https://play.devnewton.fr), le jeu de plateforme libre qui permet d'avoir le mal de mer même sans Occulus Rift.

Cette version apporte surtout des corrections de bugs et des améliorations du code, mais contient aussi quelques nouveautés:

- un menu entièrement refait avec jnuit, la ~~librairie~~ ~~bibliothèque~~ lib que j'utilise aussi pour développer [Ned et les maki](https://play.devnewton.fr/prototypes.html). Cela permets de pouvoir régler les options avec seulement un périphérique (manette, souris, écran tactile, clavier) et pas seulement avec le couple clavier/souris comme auparavant. Les options sont aussi désormais présentes sur la version web.
- le son sur sur la version web.

Pour ce dernier point, seul Chrome permet aujourd'hui d'entendre les effets sonores et les musiques. Sous Firefox, c'est peut être un bug avec l'API [playn](http://code.google.com/p/playn/) que j'utilise ou alors je me demande s'il ne faudrait pas que j'ajoute un DRM pour que ça marche...

Pour cette version, je voulais aussi améliorer la version Android, mais mon smartphone de test s'est suicidé en se jetant du haut de la poche de sa propriétaire. L'APK est toutefois disponible avec les autres [téléchargements](https://play.devnewton.fr/downloads) pour ceux qui veulent tester.

À propos d'Android, le portage de [Ned et les maki](http://geekygoblin.org/ned-et-les-maki/index.htm) a bénéficié de mon expérience sur Newton Adventure et devrait sortir avec presque un an d'avance sur la feuille de route ! 

En attendant, Nal, joue bien [librement](https://play.devnewton.fr/mods) ou [autrement](http://steamcommunity.com/sharedfiles/filedetails/?id=187107465)!


