URL:     https://linuxfr.org/users/devnewton/journaux/chiselapp-ferme-ses-portes
Title:   Chiselapp ferme ses portes
Authors: devnewton
Date:    2013-03-29T23:02:10+01:00
License: CC By-SA
Tags:    fossil
Score:   10


L'auteur du site d'hébergement de dépôts [fossil](http://www.fossil-scm.org) chiselapp.com a annoncé la [fermeture](http://www.mail-archive.com/fossil-users@lists.fossil-scm.org/msg11540.html) en mai de ce service.

C'est une grande déception pour les amateurs de ce gestionnaire de version simple et original qui intègre un bug tracker et un wiki, car chiselapp était le seul service gratuit avec [fossilrepos](http://fossilrepos.sourceforge.net/).

Le code source est toujours [disponible](https://chiselapp.com/user/james/repository/flint/index) et l'auteur a même [adopté](http://www.mail-archive.com/fossil-users@lists.fossil-scm.org/msg11558.html) une licence plus permissive pour encourager les éventuels repreneurs.

Fossil est un très bon gestionnaire de version et même s'il est très facile à héberger sur son propre serveur, il est dommage de le voir boudé par les grandes forges du marché.
