URL:     https://linuxfr.org/users/devnewton/journaux/le-cablage-enterre-innovant
Title:   Le câblage enterré innovant
Authors: devnewton
Date:    2020-10-05T11:47:25+02:00
License: CC By-SA
Tags:    sous_traitance
Score:   34


Bonjour Nal,

Je t'écris, car un opérateur* a commencé à câbler le quartier d'un de mes amis et je découvre que la pose de câble enterré connaît une révolution disruptive.

![Je sais faire](todo.jpg)

*: chut chut pas de marque.
