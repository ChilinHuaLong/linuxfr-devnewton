URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-1-9
Title:   Newton Adventure 1.9
Authors: devnewton
Date:    2013-03-04T18:52:44+01:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure, jeu, debian, ubuntu et fedora
Score:   33


Bonjour Nal,

Je t'écris pour te donner des nouvelles du développement de [Newton Adventure](https://play.devnewton.fr).

Si tu ne te souviens plus de ce à quoi ressemble ce jeu, j'ai fait un trailer&nbsp;: http://www.dailymotion.com/video/xxxaka_newton-adventure-1-9-trailer_videogames

Nouveautés
==========

Depuis mon dernier journal, voici les évolutions visibles par les joueurs&nbsp;:

- de nouveaux graphismes, notamment ceux dessinés par [Julien](http://linuxfr.org/users/julien_jorge) qui améliorent grandement les niveaux de la quête "egypt".
- un chargement plus rapide des niveaux avec une petite animation.
- l'auto-configuration des manettes de jeu.
- des paquets deb (testé sur Debian 6  et Ubuntu 12.04) et rpm (testé sur Fedora 17).
- un installeur pour les systèmes de Microsoft (testé sur sur Windows 7).
- un installeur en java, utilisable partout, même sans compte administrateur (c'est juste un dézippeur évolué&nbsp;!).
- quelques améliorations de l'interface.

Nettoyage
=========

J'ai fait un gros nettoyage du code de Newton Adventure&nbsp;:

- Il est maintenant plus facile pour les graphistes de contribuer.
- Le portage android a été supprimé en attendant de trouver un développeur motivé pour le faire renaître.
- La version _Java Web Start_ est abandonnée.

Plus facile la contribution
---------------------------

Le grand changement est une meilleure utilisation des fichiers&nbsp;: avant, un fichier *.tmx contenait toutes les informations, images comprises, ce qui rendait pénible leur édition car, pour éditer une tuile, il fallait l'extraire via l'éditeur de niveau, l'éditer avec gimp (par exemple), puis la réintégrer via l'éditeur. Pour voir les changements en jeu, il fallait aussi systématiquement recompiler. Ce fonctionnement était nécessaire pour le portage android où on n'a pas accès à un vrai système de fichier.

Maintenant, le jeu stocke ses données dans un sous dossier `data/` qui contient toutes les images au format png. On peut les éditer et lancer le jeu pour voir les changements directement.

La fin du portage android
-------------------------

Il est toujours resté à l'état de prototype, je n'ai pas beaucoup de temps pour m'en occuper et il faudrait sans doute une réécriture plutôt qu'un portage pour s'adapter à ces petites machines. Je pense qu'il vaut mieux essayer de recruter un développeur motivé pour faire une vraie version mobile/tactile de Newton Adventure en reprenant juste les données et en faisant un code neuf avec son langage et ses apis préférées.

Java Web Start aux oubliettes
-----------------------------

Java Web Start est une très bonne idée pour déployer des applications simplement, malheureusement ce n'est pas trop compatible avec l'utilisation de bibliothèques natives, dont j'ai besoin pour accéder à OpenGL et de certificats SSL autosignés&nbsp;: l'utilisateur voit des messages d'avertissements trop dissuasifs...

Le futur
========

Je travaille sur un système de [mods](http://fr.wikipedia.org/wiki/Mod_%28jeu_vid%C3%A9o%29), afin de pouvoir créer des niveaux supplémentaires et de nouveaux jeux sur la base de Newton Adventure.

C'est encore très expérimental, mais ça me permet de créer des niveaux sans "polluer" le jeu de base et de tester de nouvelles idées de _gameplay_ dont je te reparlerai bientôt...

Appel à contributions
=====================

Je cherche du monde pour m'aider à continuer à améliorer le jeu. Portage android, packaging pour les diverses distributions linux ou macosx, identification de bugs, création de graphismes ou de niveaux, traduction, promotion... Il y a beaucoup à faire pour tous les types de contributeurs&nbsp;!

Si vous êtes intéressé, contactez-moi&nbsp;: https://play.devnewton.fr/about.html

Si vous n'êtes pas intéressé, dites-le sur [l'incubateur d'excellence de linuxfr](http://linuxfr.org/board).

