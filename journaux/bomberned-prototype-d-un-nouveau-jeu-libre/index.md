URL:     https://linuxfr.org/users/devnewton/journaux/bomberned-prototype-d-un-nouveau-jeu-libre
Title:   Bomberned: prototype d'un nouveau jeu libre
Authors: devnewton
Date:    2018-12-27T22:44:13+01:00
License: CC By-SA
Tags:    phaserio, moustaki, bombe, bomberman, rambo, brunch et typescript
Score:   21


Bonjour Nal,

Après Newton Adventure et Ned et les maki, je me lance dans le développement de nouveaux jeux libres.

Ayant plusieurs projets en tête, j'ai décidé de faire des prototypes pour tester mes idées de gameplay. Le premier était [shmuprpg](https://linuxfr.org/users/devnewton/journaux/shmuprpg-prototype-d-un-nouveau-jeu-libre), le deuxième [underthief](https://linuxfr.org/users/devnewton/journaux/underthief-prototype-d-un-nouveau-jeu-libre) et voici le troisième: [bomberned](https://play.devnewton.fr/prototypes.html).

![Titre de l'image](title.png)

Il s'agit d'un jeu où s'affronte deux équipes, les neds (koalas) et les moustakis (chats) en posant des bombes à la bomberman et en se lançant des flèches explosives à la rambo.

Une IA basique permets de jouer seul, mais le jeu est pensé pour 4 joueurs, au clavier/souris et aux manettes.

![bomberned](game.png)

Sous licences libres ( MIT pour le code et CC-BY-SA pour les médias, le jeu est une application web écrite avec [Typescript](https://www.typescriptlang.org/), [brunch](https://brunch.io/) and [Phaser](http://phaser.io).

Bomberned et mes autres jeux sont tous jouables en ligne sur [mon site](https://play.devnewton.fr).

Joyeuses fêtes à Nal et aux moules !
