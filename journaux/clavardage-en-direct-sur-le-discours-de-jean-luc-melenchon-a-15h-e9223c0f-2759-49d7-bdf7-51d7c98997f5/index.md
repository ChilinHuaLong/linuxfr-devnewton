URL:     https://linuxfr.org/users/devnewton/journaux/clavardage-en-direct-sur-le-discours-de-jean-luc-melenchon-a-15h-e9223c0f-2759-49d7-bdf7-51d7c98997f5
Title:   Clavardage en direct sur le discours de Jean Luc Mélenchon à 15h
Authors: devnewton
Date:    2017-03-18T09:38:57+01:00
License: CC By-SA
Tags:    troll, social_traître, politique, expert, tribune, coincoin et france
Score:   4


Bonjour Nal,

Je t'écris pour te signaler que les analystes, [méta-experts](https://nsfw.totoz.eu/img/MetaExpert) et moules de padding seront présentes à partir de 15h sur la [tribune](https://linuxfr.org/board) pour commenter en direct le [discours de Jean Luc Mélenchon](https://18mars2017.fr/), l'un des rares candidats pro travailleurs de l'élection présidentielle français de 2017.

Rejoignez nous pour un grand moment d'incubation d'excellence à 15h:

- soit directement sur la [tribune](https://linuxfr.org/board).
- soit via un [webcoincoin de qualité](https://gb3.devnewton.fr).
- soit via un [coincoin natif](https://github.com/dguihal/quteqoin).

![JLM2017](regard_qui_tue)


