URL:     https://linuxfr.org/users/devnewton/journaux/nouvelle-css-pour-linuxfr
Title:   Nouvelle CSS pour linuxfr
Authors: devnewton
Date:    2019-08-26T12:31:52+02:00
License: CC By-SA
Tags:    css, troll et police_de_caractères
Score:   9


Bonjour Nal,

Linuxfr se cherche une nouvelle identité visuelle. Le télétype ne faisant pas consensus, j'ai commencé une nouvelle CSS qui je l'espère pourra devenir celle par défaut.

Elle se base sur le cadriciel [NES.css](https://nostalgic-css.github.io/NES.css/) et donne à notre site préféré un délicieux charme rétro.

![linuxfr seal of quality](https://framagit.org/very-old-devnewton-projects/linuxfr8bitscss/-/raw/master/screenshot.png)

[Source](https://framagit.org/very-old-devnewton-projects/linuxfr8bitscss)

J'hésite toutefois sur le choix des couleurs, est-ce qu'il ne faudrait pas avoir du texte blanc sur fond noir? Ou jaune sur fond bleu? Qu'en penses-tu?
